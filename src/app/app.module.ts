import { BrowserModule } from '@angular/platform-browser';
import {
  NgModule,
  CUSTOM_ELEMENTS_SCHEMA,
  NO_ERRORS_SCHEMA,
} from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {
  CommonModule,
  HashLocationStrategy,
  LocationStrategy,
} from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { QuoteListComponent } from './directives/quote-list/quote-list.component';
import { SvgIconsComponent } from './directives/svg-icons/svg-icons.component';
import { NgxMaskModule } from 'ngx-mask';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LeftMenuComponent } from './directives/left-menu/left-menu.component';
import { CommonPagesModule } from './commonpages/commonpages.module';
import { CommonpagesComponent } from './commonpages/commonpages.component';
import { RecaptchaModule } from 'ng-recaptcha';
import {
  HttpClient,
  HttpClientModule,
  HTTP_INTERCEPTORS,
} from '@angular/common/http';
import { NgxSpinnerModule } from 'ngx-spinner';
import { LeftmenuModule } from 'leftmenu';
import { HeaderModule } from 'header';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CustomFormsModule } from 'ngx-custom-validators';
import { ToastrModule } from 'ngx-toastr';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { SsbModule } from './ssb/ssb.module';
import { AuthGuardService } from './core/guards/auth-guard.service';
import { HttpConfigInterceptor } from './core/interceptors/httpconfig.interceptor';
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
@NgModule({
  declarations: [
    AppComponent,
    QuoteListComponent,
    SvgIconsComponent,
    LeftMenuComponent,
    CommonpagesComponent,
  ],
  imports: [
    BrowserModule,
    CommonModule,
    NgxSpinnerModule,
    BrowserAnimationsModule,
    NgSelectModule,
    FormsModule,
    AppRoutingModule,
    NgMultiSelectDropDownModule,
    ModalModule.forRoot(),
    TabsModule.forRoot(),
    AccordionModule.forRoot(),
    NgxMaskModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    CommonPagesModule,
    RecaptchaModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    LeftmenuModule,
    HeaderModule,
    FontAwesomeModule,
    CustomFormsModule,
    NgbModule,
    SsbModule,
    ToastrModule.forRoot(), // ToastrModule added,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
  providers: [
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    AuthGuardService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpConfigInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
