import { Component, OnInit } from '@angular/core';
import { version } from '../../package.json';
import { TranslateService } from '@ngx-translate/core';
import { ApiService } from './ssb/services/api.service';
import { ScriptLoaderService } from './ssb/services/script-loader/script-loader.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute, NavigationStart, Router } from '@angular/router';
export let browserRefresh = false;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass'],
})
export class AppComponent implements OnInit {
  title = 'saral-suraksha-beema';
  version = version;
  subscription: Subscription;

  constructor(
    private translate: TranslateService,
    public scriptLoader: ScriptLoaderService,
    private apiService: ApiService,
    private route: Router,
    private ActRoute: ActivatedRoute,
  ) {
    // this.apiService.checkLogin();
    console.log(`Verison ${this.version}`);
    translate.addLangs(['en', 'hi']);
    translate.setDefaultLang('en');
    this.subscription = route.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        browserRefresh = !route.navigated;
      }
    });
  }

  ngOnInit() {
    this.scriptLoader.loadCommonElements(
      this.apiService.commonPath,
      this.apiService.commonCssPath,
      'ssb.css',
    );
    if (window.location.origin.includes('localhost')) {
      this.ActRoute.queryParams.subscribe((params) => {
        console.log(params);
        for (const [key, value] of Object.entries(params)) {
          console.log(key, value);
          sessionStorage.setItem(key, value);
        }
        if (!sessionStorage.getItem('reload')) {
          setTimeout(() => {
            sessionStorage.setItem('reload', 'false');
            window.location.reload();
          }, 1000);
        }
        if (Object.keys(params).length > 0) {
          //this.product.getMasterData();
          this.route.navigate(['ssb/alleachapplication']);
        }
      });
    }
  }

  onActivate(event, ref) {
    ref.scrollTop = 0;
  }
}
