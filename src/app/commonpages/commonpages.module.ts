import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgSelectModule } from '@ng-select/ng-select';
import { CommonPagesRoutingModule } from './commonpages-routing.module';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { CovertproposalComponent } from './covertproposal/covertproposal.component';
import { ReviewproposalComponent } from './reviewproposal/reviewproposal.component';
import { NomineedetailsComponent } from './nomineedetails/nomineedetails.component';
import { AdditionaldetailsComponent } from './additionaldetails/additionaldetails.component';
import { PreviewquoteComponent } from './previewquote/previewquote.component';
import { CustomerdetailsComponent } from './customerdetails/customerdetails.component';
import { DetaillistComponent } from './detaillist/detaillist.component';
import { PaymentComponent } from './payment/payment.component';
import { HelpTutorialComponent } from './help-tutorial/help-tutorial.component';
import { NotificationComponent } from './notification/notification.component';
import { CommonNotificationComponent } from './common-notification/common-notification.component';

import { MatTableModule } from '@angular/material/table';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HelpTicketComponent } from './help-ticket/help-ticket.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { ViewTicketComponent } from './view-ticket/view-ticket.component';

@NgModule({
  declarations: [
    CovertproposalComponent,
    ReviewproposalComponent,
    NomineedetailsComponent,
    AdditionaldetailsComponent,
    PreviewquoteComponent,
    CustomerdetailsComponent,
    DetaillistComponent,
    PaymentComponent,
    HelpTutorialComponent,
    NotificationComponent,
    HelpTicketComponent,
    ViewTicketComponent,
    CommonNotificationComponent,
  ],
  imports: [
    TabsModule.forRoot(),
    CommonModule,
    CommonPagesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    NgxPaginationModule,
    MatTableModule,
    MatListModule,
    MatButtonModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class CommonPagesModule {}
