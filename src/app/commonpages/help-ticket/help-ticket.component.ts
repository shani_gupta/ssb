import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-help-ticket',
  templateUrl: './help-ticket.component.html',
  styleUrls: ['./help-ticket.component.sass'],
})
export class HelpTicketComponent implements OnInit {
  appModuleArr = ['E-Marine'];
  documentUploadForm: FormGroup;
  helpTicketForm: FormGroup;

  constructor(
    private cd: ChangeDetectorRef,
    private formBuilder: FormBuilder,
    private router: Router,
  ) {}

  ngOnInit(): void {
    this.initializeForm();
    console.log('INIT FORM:::', this.helpTicketForm.value);
  }

  initializeForm() {
    this.helpTicketForm = this.formBuilder.group({
      applicationModule: [null],
      userContact: [''],
      documentGroup: this.addDocumentGroup(),
      remarks: [''],
    });
  }

  addDocumentGroup(): FormGroup {
    return this.formBuilder.group({
      document: [''],
      documentName: [''],
      documentSize: [''],
      documentType: [''],
      remarks: [''],
    });
  }

  isNumberKey(evt: any) {
    if (
      (evt.key >= '0' && evt.key <= '9') ||
      evt.key == 'Backspace' ||
      evt.key == 'Delete' ||
      evt.key == 'ArrowLeft' ||
      evt.key == 'ArrowRight'
    ) {
      return true;
    }
    return false;
  }

  onFileChange(event) {
    let reader = new FileReader();

    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      this.setFormFileName(file);
      reader.readAsDataURL(file);

      reader.onload = () => {
        console.log('RESULT LLL:::', reader.result);
        this.cd.markForCheck();
      };
    }
  }

  setFormFileName(file) {
    this.helpTicketForm
      .get('documentGroup')
      .get('documentName')
      .setValue(file.name);
  }

  removeDocFile() {
    this.helpTicketForm.get('documentGroup').get('documentName').setValue('');
  }

  routeBack() {
    // this.router.navigate([]);
  }

  route() {
    this.router.navigate(['viewTicket']);
  }
}
