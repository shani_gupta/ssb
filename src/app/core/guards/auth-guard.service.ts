import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import { ApiService } from '../../ssb/services/api.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuardService implements CanActivate {
  constructor(private api: ApiService, public router: Router) {}

  public canActivate(route: ActivatedRouteSnapshot) {
    let role = sessionStorage.getItem('role');
    if (role) {
      if (route.data.role && route.data.role.indexOf(role) === -1) {
        window.location.href = `${this.api.url}/#/login`;
        return false;
      }
      return true;
    }
    return false;
  }
}
