import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
//import { QuoteInformationComponent } from './ssb/producer/quote-information/quote-information.component';

const routes: Routes = [
  { path: '', redirectTo: 'ssb/alleachapplication', pathMatch: 'full' },
  {
    path: 'ssb',
    loadChildren: () => import('./ssb/ssb.module').then((m) => m.SsbModule),
  },
  //{ path: 'ssb/quote/:id', component: QuoteInformationComponent },
  //{ path: '', redirectTo: 'ssb/masterInformation', pathMatch: 'full' },
  // {
  //   path: 'ssb/quoteInformation',
  //   component: QuoteInformationComponent,
  // },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      useHash: true,
      scrollPositionRestoration: 'enabled',
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
