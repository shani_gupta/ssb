import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InrcurrencyPipe } from './inrcurrency.pipe';

@NgModule({
  declarations: [InrcurrencyPipe],
  imports: [CommonModule],
  exports: [InrcurrencyPipe],
})
export class InrcurrencyModule {}
