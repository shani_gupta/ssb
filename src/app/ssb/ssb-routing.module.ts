import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotfoundComponent } from './page-notfound/page-notfound.component';
const routes: Routes = [
  { path: '', redirectTo: 'alleachapplication', pathMatch: 'full' },
  {
    path: '',
    children: [
      {
        path: 'quote',
        loadChildren: () =>
          import('../ssb/quote/quote.module').then((m) => m.QuoteModule),
      },
      {
        path: 'proposal',
        loadChildren: () =>
          import('../ssb/proposal/proposal.module').then(
            (m) => m.ProposalModule,
          ),
      },
      {
        path: 'master',
        loadChildren: () =>
          import('../ssb/master/master.module').then((m) => m.MasterModule),
      },
      {
        path: 'alleachapplication',
        loadChildren: () =>
          import('../ssb/alleachapplication/alleachapplication.module').then(
            (m) => m.AllEachApplicationModule,
          ),
      },
    ],
  },
  //PAGE NOT FOUND
  { path: '**', component: PageNotfoundComponent },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SsbRoutingModule {}
