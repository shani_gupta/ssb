import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { WhiteSpaceValidator } from 'src/app/directives/white-space.validator';
import { GlobalService } from '../../services/global.service';

@Component({
  selector: 'app-quote-information',
  templateUrl: './quote-information.component.html',
  styleUrls: ['./quote-information.component.scss'],
})
export class QuoteInformationComponent implements OnInit {
  quoteInformationForm: FormGroup;
  dropDownArr = ['Service'];

  constructor(
    private _router: Router,
    private _formBuilder: FormBuilder,
    public global: GlobalService,
  ) {}

  ngOnInit(): void {
    this.initializeForm();
  }

  initializeForm() {
    this.quoteInformationForm = this._formBuilder.group({
      cityPincode: ['', WhiteSpaceValidator.restrictWhiteSpaces],
      occupation: [null, WhiteSpaceValidator.restrictWhiteSpaces],
      annualIncome: ['', WhiteSpaceValidator.restrictWhiteSpaces],
      gstinNumber: ['', WhiteSpaceValidator.restrictWhiteSpaces],
    });
  }

  route() {
    this._router.navigate(['ssb/quote/memberDetails']);
  }

  isNumberKey(evt: any) {
    let temp = evt?.target?.value;
    if (!(temp.length > 9)) {
      if (
        (evt.key >= '0' && evt.key <= '9') ||
        evt.key == 'Backspace' ||
        evt.key == 'Delete' ||
        evt.key == 'ArrowLeft' ||
        evt.key == 'ArrowRight'
      ) {
        return true;
      }
    }
    return false;
  }
}
