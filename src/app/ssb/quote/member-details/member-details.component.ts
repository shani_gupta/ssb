import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { EventManager } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { WhiteSpaceValidator } from 'src/app/directives/white-space.validator';
import { GlobalService } from '../../services/global.service';

@Component({
  selector: 'app-member-details',
  templateUrl: './member-details.component.html',
  styleUrls: ['./member-details.component.scss'],
})
export class MemberDetailsComponent implements OnInit {
  memberDetailsForm: FormGroup;
  months: Object = {
    jan: '01',
    feb: '02',
    mar: '03',
    apr: '04',
    may: '05',
    jun: '06',
    jul: '07',
    aug: '08',
    sep: '09',
    oct: '10',
    nov: '11',
    dec: '12',
  };
  @ViewChild('date1') date1: ElementRef;
  @ViewChild('date2') date2: ElementRef;
  @ViewChild('date3') date3: ElementRef;
  @ViewChild('date4') date4: ElementRef;
  amtArr = [];
  childCount: number = 0;
  addRiderMemberObj: Object = {
    self: {
      addRiderFlag: false,
    },
    spouse: {
      addRiderFlag: false,
    },
    child_1: {
      addRiderFlag: false,
    },
    child_2: {
      addRiderFlag: false,
    },
  };

  constructor(
    private _router: Router,
    private _formBuilder: FormBuilder,
    private eventManager: EventManager,
    public global: GlobalService,
  ) {}

  ngOnInit(): void {
    this.initializeForm();
  }

  initializeForm() {
    this.memberDetailsForm = this._formBuilder.group({
      selfNotIncluded: [false],
      memberIsEmployee: [false],
      existingCustomer: [true],
      self: [true],
      spouse: [false],
      child_1: [false],
      child_2: [false],
      father: [false],
      mother: [false],
      father_in_law: [false],
      mother_in_law: [false],
      self_dob: ['', WhiteSpaceValidator.restrictWhiteSpaces],
      spouse_dob: ['', WhiteSpaceValidator.restrictWhiteSpaces],
      child_1_dob: ['', WhiteSpaceValidator.restrictWhiteSpaces],
      child_2_dob: ['', WhiteSpaceValidator.restrictWhiteSpaces],
      selfRiskClass: [null],
      spouseRiskClass: [null],
      annualIncome: ['', WhiteSpaceValidator.restrictWhiteSpaces],
      sumInsured: ['', WhiteSpaceValidator.restrictWhiteSpaces],
      occupation: ['', WhiteSpaceValidator.restrictWhiteSpaces],
      spouseAnnualIncome: ['', WhiteSpaceValidator.restrictWhiteSpaces],
      spouseSumInsured: ['', WhiteSpaceValidator.restrictWhiteSpaces],
      spouseOccupation: ['', WhiteSpaceValidator.restrictWhiteSpaces],
      child1AnnualIncome: ['', WhiteSpaceValidator.restrictWhiteSpaces],
      child1SumInsured: ['', WhiteSpaceValidator.restrictWhiteSpaces],
      child1Occupation: ['', WhiteSpaceValidator.restrictWhiteSpaces],
      child2AnnualIncome: ['', WhiteSpaceValidator.restrictWhiteSpaces],
      child2SumInsured: ['', WhiteSpaceValidator.restrictWhiteSpaces],
      child2Occupation: ['', WhiteSpaceValidator.restrictWhiteSpaces],
      child_1RiskClass: [null],
      child_2RiskClass: [null],
      selfHistoryQuesRep: [false],
      spouseHistoryQuesRep: [false],
      child_1HistoryQuesRep: [false],
      child_2HistoryQuesRep: [false],
    });
  }

  ngAfterViewInit() {
    this.preFormatDates(this.date1?.nativeElement);
  }

  routeBack() {
    this._router.navigate(['ssb/quote']);
  }

  isSelfNotIncluded() {
    if (this.memberDetailsForm.get('selfNotIncluded').value) {
      this.memberDetailsForm.get('self').setValue(false);
    } else {
      this.memberDetailsForm.get('self').setValue(true);
      setTimeout(() => {
        this.preFormatDates(this.date1.nativeElement);
      }, 100);
    }
  }

  isNumberKey(evt: any) {
    let temp = evt?.target?.value;
    if (!(temp.length > 9)) {
      if (
        (evt.key >= '0' && evt.key <= '9') ||
        evt.key == 'Backspace' ||
        evt.key == 'Delete' ||
        evt.key == 'ArrowLeft' ||
        evt.key == 'ArrowRight'
      ) {
        return true;
      }
    }
    return false;
  }

  checkValue(str, max) {
    if (str.charAt(0) !== '0' || str == '00') {
      var num = parseInt(str);
      if (isNaN(num) || num <= 0 || num > max) num = 1;
      str =
        num > parseInt(max.toString().charAt(0)) && num.toString().length == 1
          ? '0' + num
          : num.toString();
    }
    return str;
  }

  preFormatDates(htmlElement) {
    this.eventManager.addEventListener(htmlElement, 'keydown', (e) => {
      var input = htmlElement.value;
      if (/[\-]/.test(input)) return;
      var key = e.keyCode || e.charCode;

      if (key == 8 || key == 46) return false;

      if (/\D\/$/.test(input)) input = input.substr(0, input.length - 1);
      var values = input.split('/').map(function (v) {
        return v.replace(/\D/g, '');
      });
      if (values[0]) values[0] = this.checkValue(values[0], 31);
      if (values[1]) values[1] = this.checkValue(values[1], 12);
      var output = values.map(function (v, i) {
        return v.length == 2 && i < 2 ? v + '/' : v;
      });
      htmlElement.value = output.join('').substr(0, 10);
    });
  }

  formatLikeExcel(card) {
    var htmlElement = this.getHTMLElement(card);
    var values = htmlElement.value;

    var arr = values.split('-');
    // console.log("parsed value",arr,arr.length,arr[1],parseInt(arr[1]));
    if (arr.length > 1) {
      arr[1] = parseInt(arr[1])
        ? parseInt(arr[1])
        : this.months[arr[1].toLocaleLowerCase()];
      arr[2] = parseInt(arr[2]) ? parseInt(arr[2]) : '';
      var finalValue = arr.join('/');
      htmlElement.value = finalValue;
    }
  }

  getHTMLElement(card) {
    var htmlElement;
    switch (card) {
      case 'self': {
        htmlElement = this.date1.nativeElement;
        break;
      }
      case 'spouse': {
        htmlElement = this.date2.nativeElement;
        break;
      }
      case 'child_1': {
        htmlElement = this.date3.nativeElement;
        break;
      }
      case 'child_2': {
        htmlElement = this.date4.nativeElement;
        break;
      }
    }
    return htmlElement;
  }

  relationshipCheckBox(rel) {
    switch (rel) {
      case 'self': {
        // this.memberDetailsForm.get('self').setValue(false);
        break;
      }
      case 'spouse': {
        if (this.memberDetailsForm.get('spouse').value) {
          setTimeout(() => {
            this.preFormatDates(this.date2.nativeElement);
          }, 100);
        }
        break;
      }
      case 'child_1': {
        if (this.memberDetailsForm.get('child_1').value) {
          setTimeout(() => {
            this.preFormatDates(this.date3.nativeElement);
          }, 100);
          this.childCount += 1;
        } else {
          this.childCount = this.childCount > 0 ? this.childCount - 1 : 0;
          if (this.memberDetailsForm.get('child_2').value) {
            this.memberDetailsForm.get('child_2').setValue(false);
            this.childCount = this.childCount > 0 ? this.childCount - 1 : 0;
          }
        }
        break;
      }
    }
  }

  selfChecked() {
    if (this.memberDetailsForm.get('self').value) {
      this.memberDetailsForm.get('selfNotIncluded').setValue(false);
    }
  }

  closeCard(card) {
    switch (card) {
      case 'self': {
        this.memberDetailsForm.get('self').setValue(false);
        break;
      }
      case 'spouse': {
        this.memberDetailsForm.get('spouse').setValue(false);
        break;
      }
      case 'child_1': {
        this.memberDetailsForm.get('child_1').setValue(false);
        this.childCount = this.childCount > 0 ? this.childCount - 1 : 0;
        if (this.memberDetailsForm.get('child_2').value) {
          this.memberDetailsForm.get('child_2').setValue(false);
          this.childCount = this.childCount > 0 ? this.childCount - 1 : 0;
        }
        break;
      }
      case 'child_2': {
        this.memberDetailsForm.get('child_2').setValue(false);
        this.childCount = this.childCount > 0 ? this.childCount - 1 : 0;
        break;
      }
    }
    // console.log("CARDTYPE:::::::",cardType);
  }

  addAnotherChildBtn() {
    this.memberDetailsForm.get('child_2').setValue(true);
    setTimeout(() => {
      this.preFormatDates(this.date4.nativeElement);
    }, 100);
    this.childCount += 1;
  }

  addRiderBtn(member) {
    console.log(
      'MEMBER',
      member,
      ':::::::',
      this.addRiderMemberObj[member]['addRiderFlag'],
    );
    this.addRiderMemberObj[member]['addRiderFlag'] =
      !this.addRiderMemberObj[member]['addRiderFlag'];
    console.log(
      'MEMBER',
      member,
      ':::::::',
      this.addRiderMemberObj[member]['addRiderFlag'],
    );
  }

  route() {
    this._router.navigate(['ssb/quote/allPremiums']);
  }
}
