import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllPremiumsComponent } from './all-premiums/all-premiums.component';
import { MemberDetailsComponent } from './member-details/member-details.component';
import { PreviewQuoteComponent } from './preview-quote/preview-quote.component';
import { QuoteInformationComponent } from './quote-information/quote-information.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '',
    pathMatch: 'full',
    component: QuoteInformationComponent,
  },
  // {
  //   path: ':id',
  //   pathMatch: 'full',
  //   component: QuoteInformationComponent,
  // },
  {
    path: 'memberDetails',
    pathMatch: 'full',
    component: MemberDetailsComponent,
    //canActivate: [FormGuardService],
  },
  // {
  //   path: 'memberDetails/:id',
  //   pathMatch: 'full',
  //   component: MemberDetailsComponent,
  // },
  {
    path: 'allPremiums',
    pathMatch: 'full',
    component: AllPremiumsComponent,
    //canActivate: [FormGuardService],
  },
  // {
  //   path: 'allPremiums/:id',
  //   pathMatch: 'full',
  //   component: AllPremiumsComponent,
  //   //canActivate: [FormGuardService],
  // },
  // {
  //   path: 'quotelisting',
  //   component: QuoteListingComponent,
  // },
  {
    path: 'previewQuote',
    component: PreviewQuoteComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class QuoteRoutingModule {}
