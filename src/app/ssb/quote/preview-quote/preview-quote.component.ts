import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { GlobalService } from '../../services/global.service';

@Component({
  selector: 'app-preview-quote',
  templateUrl: './preview-quote.component.html',
  styleUrls: ['./preview-quote.component.scss'],
})
export class PreviewQuoteComponent implements OnInit {
  successModalRef: BsModalRef;
  config: any = {
    backdrop: 'static',
    keyboard: false,
  };

  constructor(
    private modalService: BsModalService,
    private router: Router,
    public global: GlobalService,
  ) {}

  ngOnInit(): void {
    this.config['class'] = 'theme-modal';
  }

  shareQuote(success_template: TemplateRef<any>) {
    this.successModalRef = this.modalService.show(
      success_template,
      this.config,
    );
  }

  routeBack() {
    this.router.navigate(['ssb/quoteListing']);
  }

  route() {
    this.router.navigate(['ssb/convertToProposal']);
  }
}
