import {
  CUSTOM_ELEMENTS_SCHEMA,
  NgModule,
  NO_ERRORS_SCHEMA,
} from '@angular/core';
import { CommonModule } from '@angular/common';

import { QuoteRoutingModule } from './quote-routing.module';

import { AllPremiumsComponent } from './all-premiums/all-premiums.component';
import { PreviewQuoteComponent } from './preview-quote/preview-quote.component';
import { QuoteInformationComponent } from './quote-information/quote-information.component';
import { MemberDetailsComponent } from './member-details/member-details.component';
import { QuoteListingComponent } from './quote-listing/quote-listing.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrModule } from 'ngx-toastr';
import { TranslateModule } from '@ngx-translate/core';
import { ServicesModule } from '../services/services.module';

@NgModule({
  declarations: [
    QuoteInformationComponent,
    AllPremiumsComponent,
    PreviewQuoteComponent,
    MemberDetailsComponent,
    QuoteListingComponent,
  ],
  imports: [
    CommonModule,
    QuoteRoutingModule,
    NgSelectModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    NgMultiSelectDropDownModule,
    NgxPaginationModule,
    ToastrModule.forRoot({ timeOut: 10000, preventDuplicates: true }),
    TranslateModule,
    ServicesModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class QuoteModule {}
