import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { EventManager } from '@angular/platform-browser';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { WhiteSpaceValidator } from 'src/app/directives/white-space.validator';
import { GlobalService } from '../../services/global.service';

@Component({
  selector: 'app-nominee-details',
  templateUrl: './nominee-details.component.html',
  styleUrls: ['./nominee-details.component.scss'],
})
export class NomineeDetailsComponent implements OnInit {
  nomineeDetailsForm: FormGroup;
  months: Object = {
    jan: '01',
    feb: '02',
    mar: '03',
    apr: '04',
    may: '05',
    jun: '06',
    jul: '07',
    aug: '08',
    sep: '09',
    oct: '10',
    nov: '11',
    dec: '12',
  };
  dropDownArr = [];

  constructor(
    private _router: Router,
    private _formBuilder: FormBuilder,
    private eventManager: EventManager,
    public global: GlobalService,
  ) {}

  ngOnInit(): void {
    this.initializeForm();
  }

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    const nomineeArr = this.nomineeDetailsForm.get('nomineeArr') as FormArray;
    let arrLength = nomineeArr.length;
    let dateCombArr = this.getDateCombinationArrSF(arrLength);
    dateCombArr.forEach((ele) => {
      this.preFormatDates(document.getElementById(ele));
    });
  }

  initializeForm() {
    this.nomineeDetailsForm = this._formBuilder.group({
      nomineeArr: this._formBuilder.array([this.addNomineeGroup()]),
    });
  }

  addNomineeGroup(): FormGroup {
    return this._formBuilder.group({
      nomineeDetails: this._formBuilder.group({
        name: ['', WhiteSpaceValidator.restrictWhiteSpaces],
        dateOfBirth: ['', WhiteSpaceValidator.restrictWhiteSpaces],
        gender: [null, WhiteSpaceValidator.restrictWhiteSpaces],
        relationshipWithProposer: [
          null,
          WhiteSpaceValidator.restrictWhiteSpaces,
        ],
        nomineeAddress: ['', WhiteSpaceValidator.restrictWhiteSpaces],
        percentageShare: ['', WhiteSpaceValidator.restrictWhiteSpaces],
      }),
      appointeeDetails: this._formBuilder.group({
        appointeeName: ['', WhiteSpaceValidator.restrictWhiteSpaces],
        relationshipWithNominee: [
          null,
          WhiteSpaceValidator.restrictWhiteSpaces,
        ],
        appointeeAddress: ['', WhiteSpaceValidator.restrictWhiteSpaces],
      }),
      displayAppointeeFlag: [false, WhiteSpaceValidator.restrictWhiteSpaces],
    });
  }

  isNumberKey(evt: any) {
    let temp = evt?.target?.value;
    if (!(temp.length > 9)) {
      if (
        (evt.key >= '0' && evt.key <= '9') ||
        evt.key == 'Backspace' ||
        evt.key == 'Delete' ||
        evt.key == 'ArrowLeft' ||
        evt.key == 'ArrowRight'
      ) {
        return true;
      }
    }
    return false;
  }

  formatName(evt: any) {
    var regex = new RegExp('^[a-zA-Z s]+$');
    var str = String.fromCharCode(!evt.charCode ? evt.which : evt.charCode);
    if (regex.test(str)) {
      return true;
    } else {
      evt.preventDefault();
      return false;
    }
  }

  checkValue(str, max) {
    if (str.charAt(0) !== '0' || str == '00') {
      var num = parseInt(str);
      if (isNaN(num) || num <= 0 || num > max) num = 1;
      str =
        num > parseInt(max.toString().charAt(0)) && num.toString().length == 1
          ? '0' + num
          : num.toString();
    }
    return str;
  }

  preFormatDates(htmlElement) {
    this.eventManager.addEventListener(htmlElement, 'keydown', (e) => {
      var input = htmlElement.value;
      if (/[\-]/.test(input)) return;
      var key = e.keyCode || e.charCode;

      if (key == 8 || key == 46) return false;

      if (/\D\/$/.test(input)) input = input.substr(0, input.length - 1);
      var values = input.split('/').map(function (v) {
        return v.replace(/\D/g, '');
      });
      if (values[0]) values[0] = this.checkValue(values[0], 31);
      if (values[1]) values[1] = this.checkValue(values[1], 12);
      var output = values.map(function (v, i) {
        return v.length == 2 && i < 2 ? v + '/' : v;
      });
      htmlElement.value = output.join('').substr(0, 10);
    });
  }

  formatLikeExcel(nominee, dateControl) {
    var values = nominee.get('nomineeDetails').get(dateControl).value;

    var arr = values.split('-');
    // console.log("parsed value",arr,arr.length,arr[1],parseInt(arr[1]));
    if (arr.length > 1) {
      arr[1] = parseInt(arr[1])
        ? parseInt(arr[1])
        : this.months[arr[1].toLocaleLowerCase()];
      arr[2] = parseInt(arr[2]) ? parseInt(arr[2]) : '';
      var finalValue = arr.join('/');
      // htmlElement.value = finalValue;
      nominee.get('nomineeDetails').get(dateControl).setValue(finalValue);
    } else {
      var arr = values.split('/');
      if (arr.length > 1) {
        arr[1] = parseInt(arr[1])
          ? parseInt(arr[1])
          : this.months[arr[1].toLocaleLowerCase()];
        arr[2] = parseInt(arr[2]) ? parseInt(arr[2]) : '';
        var finalValue = arr.join('/');
        nominee.get('nomineeDetails').get(dateControl).setValue(finalValue);
      }
    }
    this.calculateMemberNomineeAge(nominee);
  }

  getDateCombinationArrSF(length) {
    console.log('IN LOOP ::::::', ':::::::::', length);
    let dateCombArr = [];
    for (var i = 0; i < length; i++) {
      dateCombArr.push('dateOfBirth' + i);
    }
    console.log('COMBINATION ARR::::', dateCombArr);
    return dateCombArr;
  }

  calculateMemberNomineeAge(member) {
    const value = member.get('nomineeDetails').get('dateOfBirth').value;
    if (value.length == 10) {
      let year = moment().diff(moment(value, 'DD/MM/YYYY'), 'years');
      let months = moment().diff(moment(value, 'DD/MM/YYYY'), 'months');
      let days = moment().diff(moment(value, 'DD/MM/YYYY'), 'days');
      console.log(
        'AGE IN YEARS::::::::::::',
        year,
        ':::::::::::',
        months,
        '::::::::::::::',
        days,
      );
      if (year > 0 && year < 18) {
        member.get('displayAppointeeFlag').setValue(true);
        console.log('TRUE::::::::::', member);
      } else if (year == 0) {
        if ((months && months > 0) || (days && days > 0)) {
          member.get('displayAppointeeFlag').setValue(true);
          console.log('TRUE::::::::::', member);
        }
      }
    }
  }

  addNominee() {
    const nomineeArr = this.nomineeDetailsForm.get('nomineeArr') as FormArray;
    nomineeArr.push(this.addNomineeGroup());
    this.formatAddedDates();
  }

  formatAddedDates() {
    const nomineeArr = this.nomineeDetailsForm.get('nomineeArr') as FormArray;
    let arrLength = nomineeArr.length;
    let dateCombArr = this.getDateCombinationArrSF(arrLength);
    setTimeout(() => {
      dateCombArr.forEach((ele) => {
        this.preFormatDates(document.getElementById(ele));
      });
    }, 100);
  }

  removeNomineeArrBtn(ind) {
    console.log('ind', ind);
    const nomineeArr = this.nomineeDetailsForm.get('nomineeArr') as FormArray;
    nomineeArr.removeAt(ind);
  }

  routeBack() {
    this._router.navigate(['ssb/memberProposalDetails']);
  }

  route() {
    this._router.navigate(['ssb/additionalPolicyDetails']);
  }

  routeBackToProposal() {
    this._router.navigate(['ssb/convertToProposal']);
  }

  // numericCheck(event) {
  //   this.nomineeDetailsForm?.controls?.percentageShare?.setValue(
  //     event?.target?.value.replace(/[^0-9]/g, ''),
  //   );
  // }
}
