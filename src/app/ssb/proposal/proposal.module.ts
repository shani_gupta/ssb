import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProposalRoutingModule } from './proposal-routing.module';
import { ConvertToProposalComponent } from './convert-to-proposal/convert-to-proposal.component';
import { AdditionalDetailsComponent } from './additional-details/additional-details.component';
import { AdditionalPolicyDetailsComponent } from './additional-policy-details/additional-policy-details.component';
import { CustomerDetailsComponent } from './customer-details/customer-details.component';
import { DocumentUploadComponent } from './document-upload/document-upload.component';
import { MemberProposalDetailsComponent } from './member-proposal-details/member-proposal-details.component';
import { NomineeDetailsComponent } from './nominee-details/nominee-details.component';
import { ReviewProposalComponent } from './review-proposal/review-proposal.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ToastrModule } from 'ngx-toastr';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    ConvertToProposalComponent,
    AdditionalDetailsComponent,
    AdditionalPolicyDetailsComponent,
    CustomerDetailsComponent,
    DocumentUploadComponent,
    MemberProposalDetailsComponent,
    NomineeDetailsComponent,
    ReviewProposalComponent,
  ],
  imports: [
    CommonModule,
    ProposalRoutingModule,
    NgSelectModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    NgMultiSelectDropDownModule,
    NgxSpinnerModule,
    ToastrModule.forRoot({
      timeOut: 10000,
      preventDuplicates: true,
    }),
    TranslateModule,
  ],
})
export class ProposalModule {}
