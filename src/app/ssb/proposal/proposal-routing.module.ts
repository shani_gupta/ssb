import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdditionalDetailsComponent } from './additional-details/additional-details.component';
import { AdditionalPolicyDetailsComponent } from './additional-policy-details/additional-policy-details.component';
import { ConvertToProposalComponent } from './convert-to-proposal/convert-to-proposal.component';
import { CustomerDetailsComponent } from './customer-details/customer-details.component';
import { DocumentUploadComponent } from './document-upload/document-upload.component';
import { MemberProposalDetailsComponent } from './member-proposal-details/member-proposal-details.component';
import { NomineeDetailsComponent } from './nominee-details/nominee-details.component';
import { ReviewProposalComponent } from './review-proposal/review-proposal.component';

const routes: Routes = [
  {
    path: 'convertToProposal/:id',
    pathMatch: 'full',
    component: ConvertToProposalComponent,
  },
  // {
  //   path: 'produceGeneralDetails/:id',
  //   pathMatch: 'full',
  //   component: ProducerGeneralDetailsComponent,
  // },
  {
    path: 'customerDetails/:id',
    pathMatch: 'full',
    component: CustomerDetailsComponent,
  },
  {
    path: 'memberProposalDetails/:id',
    pathMatch: 'full',
    component: MemberProposalDetailsComponent,
  },
  {
    path: 'nomineeDetails/:id',
    pathMatch: 'full',
    component: NomineeDetailsComponent,
  },
  {
    path: 'additionalPolicyDetails/:id',
    pathMatch: 'full',
    component: AdditionalPolicyDetailsComponent,
  },
  {
    path: 'additionalDetails/:id',
    pathMatch: 'full',
    component: AdditionalDetailsComponent,
  },
  {
    path: 'documentUpload/:id',
    pathMatch: 'full',
    component: DocumentUploadComponent,
  },
  {
    path: 'reviewProposal/:id',
    pathMatch: 'full',
    component: ReviewProposalComponent,
  },
  // {
  //   path: 'underwriter-discrepancy/:id',
  //   pathMatch: 'full',
  //   component: UwDiscrepancyComponent,
  // },
  // {
  //   path: 'proposallisting',
  //   component: ProposalListingComponent,
  // },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProposalRoutingModule {}
