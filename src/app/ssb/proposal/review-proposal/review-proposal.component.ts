import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { GlobalService } from '../../services/global.service';

@Component({
  selector: 'app-review-proposal',
  templateUrl: './review-proposal.component.html',
  styleUrls: ['./review-proposal.component.scss'],
})
export class ReviewProposalComponent implements OnInit {
  successModalRef: BsModalRef;
  config: any = {
    backdrop: 'static',
    keyboard: false,
  };
  isProposalFinalised: boolean = false;

  constructor(
    private modalService: BsModalService,
    private router: Router,
    public global: GlobalService,
  ) {}

  ngOnInit(): void {
    this.config['class'] = 'theme-modal';
  }

  shareQuote(success_template: TemplateRef<any>) {
    this.successModalRef = this.modalService.show(
      success_template,
      this.config,
    );
  }

  routeBack() {
    if (this.isProposalFinalised) {
      this.router.navigate(['ssb/quoteListing']);
    } else {
      this.router.navigate(['ssb/convertToProposal']);
    }
  }

  finalizeProposal() {
    this.isProposalFinalised = true;
  }
}
