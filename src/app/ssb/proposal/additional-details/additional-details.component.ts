import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { WhiteSpaceValidator } from 'src/app/directives/white-space.validator';
import { GlobalService } from '../../services/global.service';

@Component({
  selector: 'app-additional-details',
  templateUrl: './additional-details.component.html',
  styleUrls: ['./additional-details.component.scss'],
})
export class AdditionalDetailsComponent implements OnInit {
  additionalDetailsForm: FormGroup;

  constructor(
    private _router: Router,
    private _formBuilder: FormBuilder,
    public global: GlobalService,
  ) {}

  ngOnInit(): void {
    this.initializeForm();
  }

  initializeForm() {
    this.additionalDetailsForm = this._formBuilder.group({
      customerPanNumber: ['', WhiteSpaceValidator.restrictWhiteSpaces],
      customerGstinNo: ['', WhiteSpaceValidator.restrictWhiteSpaces],
    });
  }

  routeBack() {
    this._router.navigate(['ssb/additionalPolicyDetails']);
  }

  route() {
    this._router.navigate(['ssb/documentUpload']);
  }

  routeBackToProposal() {
    this._router.navigate(['ssb/convertToProposal']);
  }

  isNumberKey(evt: any) {
    let temp = evt?.target?.value;
    if (!(temp.length > 9)) {
      if (
        (evt.key >= '0' && evt.key <= '9') ||
        evt.key == 'Backspace' ||
        evt.key == 'Delete' ||
        evt.key == 'ArrowLeft' ||
        evt.key == 'ArrowRight'
      ) {
        return true;
      }
    }
    return false;
  }
}
