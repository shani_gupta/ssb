import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { EventManager } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { WhiteSpaceValidator } from 'src/app/directives/white-space.validator';
import { GlobalService } from '../../services/global.service';

@Component({
  selector: 'app-customer-details',
  templateUrl: './customer-details.component.html',
  styleUrls: ['./customer-details.component.scss'],
})
export class CustomerDetailsComponent implements OnInit {
  customerDetailsForm: FormGroup;
  dropDownArr = [];
  months: Object = {
    jan: '01',
    feb: '02',
    mar: '03',
    apr: '04',
    may: '05',
    jun: '06',
    jul: '07',
    aug: '08',
    sep: '09',
    oct: '10',
    nov: '11',
    dec: '12',
  };
  @ViewChild('dateOfBirth') dateOfBirth: ElementRef;

  constructor(
    private _router: Router,
    private _formBuilder: FormBuilder,
    private eventManager: EventManager,
    private cd: ChangeDetectorRef,
    public global: GlobalService,
  ) {}

  ngOnInit(): void {
    this.initializeForm();
  }

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    this.preFormatDates(this.dateOfBirth.nativeElement);
  }

  initializeForm() {
    this.customerDetailsForm = this._formBuilder.group({
      salutation: [null, WhiteSpaceValidator.restrictWhiteSpaces],
      firstName: ['', WhiteSpaceValidator.restrictWhiteSpaces],
      middleName: ['', WhiteSpaceValidator.restrictWhiteSpaces],
      lastName: ['', WhiteSpaceValidator.restrictWhiteSpaces],
      dateOfBirth: ['', WhiteSpaceValidator.restrictWhiteSpaces],
      gender: [null, WhiteSpaceValidator.restrictWhiteSpaces],
      maritalStatus: [null, WhiteSpaceValidator.restrictWhiteSpaces],
      occupation: [null, WhiteSpaceValidator.restrictWhiteSpaces],
      affiliationType: [null, WhiteSpaceValidator.restrictWhiteSpaces],
      companyName: ['', WhiteSpaceValidator.restrictWhiteSpaces],
      companyLocation: ['', WhiteSpaceValidator.restrictWhiteSpaces],
      companyAddress: ['', WhiteSpaceValidator.restrictWhiteSpaces],
      employeeID: ['', WhiteSpaceValidator.restrictWhiteSpaces],
      uploadImage: this._formBuilder.group({
        image: ['', WhiteSpaceValidator.restrictWhiteSpaces],
        imageName: ['', WhiteSpaceValidator.restrictWhiteSpaces],
        imageType: ['', WhiteSpaceValidator.restrictWhiteSpaces],
        imageSize: ['', WhiteSpaceValidator.restrictWhiteSpaces],
      }),
      contactDetails: this._formBuilder.group({
        emialID: ['', WhiteSpaceValidator.restrictWhiteSpaces],
        mobileNumber: ['', WhiteSpaceValidator.restrictWhiteSpaces],
      }),
      communicationAddress: this._formBuilder.group({
        pincode: ['', WhiteSpaceValidator.restrictWhiteSpaces],
        addressLine_1: ['', WhiteSpaceValidator.restrictWhiteSpaces],
        addressLine_2: ['', WhiteSpaceValidator.restrictWhiteSpaces],
        addressLine_3: ['', WhiteSpaceValidator.restrictWhiteSpaces],
        townCity: ['', WhiteSpaceValidator.restrictWhiteSpaces],
        state: [null, WhiteSpaceValidator.restrictWhiteSpaces],
      }),
      existingCustomer: [false, WhiteSpaceValidator.restrictWhiteSpaces],
      existingRelationshipDetails: [
        '',
        WhiteSpaceValidator.restrictWhiteSpaces,
      ],
      customerProposer: [false, WhiteSpaceValidator.restrictWhiteSpaces],
    });
  }

  isNumberKey(evt: any) {
    let temp = evt?.target?.value;
    if (!(temp.length > 9)) {
      if (
        (evt.key >= '0' && evt.key <= '9') ||
        evt.key == 'Backspace' ||
        evt.key == 'Delete' ||
        evt.key == 'ArrowLeft' ||
        evt.key == 'ArrowRight'
      ) {
        return true;
      }
    }
    return false;
  }

  formatName(evt: any) {
    var regex = new RegExp('^[a-zA-Z s]+$');
    var str = String.fromCharCode(!evt.charCode ? evt.which : evt.charCode);
    if (regex.test(str)) {
      return true;
    } else {
      evt.preventDefault();
      return false;
    }
  }

  formatEmail(evt: any) {
    var regex = /[A-Z0-9a-z@\._-]/;
    var str = String.fromCharCode(!evt.charCode ? evt.which : evt.charCode);
    if (regex.test(str)) {
      return true;
    } else {
      evt.preventDefault();
      return false;
    }
  }

  checkValue(str, max) {
    if (str.charAt(0) !== '0' || str == '00') {
      var num = parseInt(str);
      if (isNaN(num) || num <= 0 || num > max) num = 1;
      str =
        num > parseInt(max.toString().charAt(0)) && num.toString().length == 1
          ? '0' + num
          : num.toString();
    }
    return str;
  }

  preFormatDates(htmlElement) {
    this.eventManager.addEventListener(htmlElement, 'keydown', (e) => {
      var input = htmlElement.value;
      if (/[\-]/.test(input)) return;
      var key = e.keyCode || e.charCode;

      if (key == 8 || key == 46) return false;

      if (/\D\/$/.test(input)) input = input.substr(0, input.length - 1);
      var values = input.split('/').map(function (v) {
        return v.replace(/\D/g, '');
      });
      if (values[0]) values[0] = this.checkValue(values[0], 31);
      if (values[1]) values[1] = this.checkValue(values[1], 12);
      var output = values.map(function (v, i) {
        return v.length == 2 && i < 2 ? v + '/' : v;
      });
      htmlElement.value = output.join('').substr(0, 10);
    });
  }

  formatLikeExcel(dateControl) {
    var values = this.customerDetailsForm.get(dateControl).value;

    var arr = values.split('-');
    // console.log("parsed value",arr,arr.length,arr[1],parseInt(arr[1]));
    if (arr.length > 1) {
      arr[1] = parseInt(arr[1])
        ? parseInt(arr[1])
        : this.months[arr[1].toLocaleLowerCase()];
      arr[2] = parseInt(arr[2]) ? parseInt(arr[2]) : '';
      var finalValue = arr.join('/');
      // htmlElement.value = finalValue;
      this.customerDetailsForm.get(dateControl).setValue(finalValue);
    } else {
      var arr = values.split('/');
      if (arr.length > 1) {
        arr[1] = parseInt(arr[1])
          ? parseInt(arr[1])
          : this.months[arr[1].toLocaleLowerCase()];
        arr[2] = parseInt(arr[2]) ? parseInt(arr[2]) : '';
        var finalValue = arr.join('/');
        this.customerDetailsForm.get(dateControl).setValue(finalValue);
      }
    }
  }

  onFileChange(event) {
    let reader = new FileReader();

    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      this.setFormFileName(file);
      reader.readAsDataURL(file);

      reader.onload = () => {
        this.cd.markForCheck();
      };
    }
  }

  setFormFileName(file) {
    this.customerDetailsForm
      .get('uploadImage')
      .get('imageName')
      .setValue(file.name);
  }

  removeDocFile() {
    this.customerDetailsForm.get('uploadImage').get('imageName').setValue('');
  }
  routeBack() {
    this.routeBackToProposal();
  }

  route() {
    this._router.navigate(['ssb/memberProposalDetails']);
  }

  routeBackToProposal() {
    this._router.navigate(['ssb/convertToProposal']);
  }
}
