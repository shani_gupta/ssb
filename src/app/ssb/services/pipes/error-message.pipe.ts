import { Pipe, PipeTransform } from '@angular/core';
import { checkFormControl } from '../utils/FormValidation';
@Pipe({
  name: 'ErrorMessagePipe',
})
export class ErrorMessagePipe implements PipeTransform {
  transform(value, control, touched, dirty) {
    let newErrorValue;
    newErrorValue = checkFormControl(control);
    return newErrorValue;
  }
}
