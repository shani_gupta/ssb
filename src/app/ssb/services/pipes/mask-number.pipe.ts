import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'maskNumber',
})
export class MaskNumberPipe implements PipeTransform {
  transform(number) {
    let numberArr = number.split('');
    let lastFourDigit = numberArr
      .splice(numberArr.length - 4, numberArr.length)
      .join('');
    let maskedNumber = 'XXXXXX' + lastFourDigit;

    return maskedNumber;
  }
}
