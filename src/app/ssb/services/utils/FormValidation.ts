import {
  AbstractControl,
  FormArray,
  FormControl,
  FormGroup,
  ValidationErrors,
} from '@angular/forms';
import * as moment from 'moment';
import {
  errorMapperPattern,
  errorMapperRequired,
  errorMessage,
} from '../constants/error-message';
// import { errorMapperRequired, errorMessage } from '../../../shared/constants/error-message';
// import { errorMapperPattern } from '../../../shared/constants/error-message';

export function checkFormControl(control: AbstractControl): string {
  if ((control.touched || control.dirty) && control.invalid && control.errors) {
    const errors: ValidationErrors = control.errors;
    const name = getControlName(control);
    if (errors.required) {
      return errorMapperRequired[name]
        ? errorMapperRequired[name]
        : errorMessage.required;
    }
    if (errors.pattern) {
      return errorMapperPattern[name]
        ? errorMapperPattern[name]
        : errorMessage.pattern;
    }
    if (errors.minlength) {
      return errorMessage.minLength + errors.minlength.requiredLength;
    }
    if (errors.maxlength) {
      return errorMessage.maxLength + errors.maxlength.requiredLength;
    }
    if (errors.min) {
      return errorMessage.minValue + errors.min.value;
    }
    if (errors.max) {
      return errorMessage.maxValue + errors.max.value;
    }
    if (errors.email) {
      // return errorMessage.email;
    }
    if (errors.invalidPolicyDate) {
      return errorMessage.invalidPolicyDate;
    }
    if (errors.invalidDobDate) {
      return errorMessage.invalidDobDate;
    }
    if (errors.invalidDate) {
      return errorMessage.invalidDate;
    }
    if (errors.pattern) {
      return errorMessage.pattern;
    }
    if (errors.invalidGstStateCode) {
      return errorMessage.invalidGstStateCode;
    }
    if (errors.invalidPanGst) {
      return errorMessage.invalidPanGst;
    }
    if (errors.invalidPincode) {
      return errorMessage.invalidPincode;
    }
    if (errors.minAge) {
      return errorMessage.minAge;
    }
    if (errors.maxAge) {
      return errorMessage.maxAge;
    }
    if (errors.childMinAge) {
      return errorMessage.childMinAge;
    }
    if (errors.childMaxAge) {
      return errorMessage.childMaxAge;
    }
    if (errors.invalidMinMaxLimit) {
      return errorMessage.invalidMinMaxLimit;
    }
    if (errors.isPastDate) {
      return errorMessage.isPastDate;
    }
  }
}

export function marksControlsTouched(group: FormGroup | FormArray): null {
  for (const i in group.controls) {
    if (i) {
      group.controls[i].markAsTouched();
      if (
        group.controls[i] instanceof FormGroup ||
        group.controls[i] instanceof FormArray
      ) {
        marksControlsTouched(group.controls[i]);
      }
    }
  }
  return;
}

export function isNumberKey(evt: any): boolean {
  if (
    (evt.key >= '0' && evt.key <= '9') ||
    evt.key == 'Backspace' ||
    evt.key == 'Delete' ||
    evt.key == 'ArrowLeft' ||
    evt.key == 'ArrowRight'
  ) {
    return true;
  }
  return false;
}

export function getControlName(c: AbstractControl): string | null {
  const formGroup = c.parent.controls;
  return Object.keys(formGroup).find((name) => c === formGroup[name]) || null;
}

export function validDob(control: FormControl): { [s: string]: boolean } {
  if (control.value && control.value.length === 10) {
    let dateNow = new Date();
    let dobDate = moment(control.value, 'DD/MM/YYYY').toDate();

    // The birth date - be sure to parse in the same format you're expecting:
    const dob = moment(control.value, 'DD/MM/YYYY');

    // The constant number of years you don't want to exceed
    const maxAge = 70;

    // Clone the dob (to not mutate it), increment by the years, and compare with day precision
    const isValidDate = dob
      .clone()
      .add(maxAge, 'years')
      .isSameOrBefore(moment(), 'day');

    if (dobDate.getTime() > dateNow.getTime() || isValidDate) {
      return { invalidDobDate: true };
    } else {
      return null;
    }
  } else {
    return null;
  }
}

export function dateValidation(control: FormControl): { [s: string]: boolean } {
  if (control.value) {
    if (control.value.length < 10) {
      return { invalidDate: true };
    }
  } else {
    return null;
  }
}

export function isDatePast(control: FormControl): { [s: string]: boolean } {
  if (control.value && control.value.length === 10) {
    let dateNow = new Date();
    let date = moment(control.value, 'DD/MM/YYYY');
    const isValidDate = date.isBefore(moment(), 'day');
    if (isValidDate) {
      return { isPastDate: true };
    } else {
      return null;
    }
  }
}

export function minAgeValid(control: FormControl): { [s: string]: boolean } {
  if (control.value && control.value.length === 10) {
    let dateNow = new Date();
    let dobDate = moment(control.value, 'DD/MM/YYYY').toDate();
    // The birth date - be sure to parse in the same format you're expecting:
    const dob = moment(control.value, 'DD/MM/YYYY');
    // The constant number of years you don't want to exceed
    const minAge = 18;

    // Clone the dob (to not mutate it), increment by the years, and compare with day precision
    const isValidDate = dob
      .clone()
      .add(minAge, 'years')
      .isSameOrAfter(moment(), 'day');

    if (dobDate.getTime() > dateNow.getTime() || isValidDate) {
      return { minAge: true };
    } else {
      return null;
    }
  } else {
    return null;
  }
}

export function maxAgeValid(control: FormControl): { [s: string]: boolean } {
  if (control.value && control.value.length === 10) {
    let dateNow = new Date();
    let dobDate = moment(control.value, 'DD/MM/YYYY').toDate();
    // The birth date - be sure to parse in the same format you're expecting:
    const dob = moment(control.value, 'DD/MM/YYYY');
    // The constant number of years you don't want to exceed
    const maxAge = 70;

    // Clone the dob (to not mutate it), increment by the years, and compare with day precision
    const isValidDate = dob
      .clone()
      .add(maxAge, 'years')
      .isSameOrBefore(moment(), 'day');

    if (dobDate.getTime() > dateNow.getTime() || isValidDate) {
      return { maxAge: true };
    } else {
      return null;
    }
  } else {
    return null;
  }
}

export function minChildAgeValid(control: FormControl): {
  [s: string]: boolean;
} {
  if (control.value && control.value.length === 10) {
    let dateNow = new Date();
    let dobDate = moment(control.value, 'DD/MM/YYYY').toDate();
    // The birth date - be sure to parse in the same format you're expecting:
    const dob = moment(control.value, 'DD/MM/YYYY');
    // The constant number of years you don't want to exceed
    const minAge = 6;

    // Clone the dob (to not mutate it), increment by the years, and compare with day precision
    const isValidDate = dob
      .clone()
      .add(minAge, 'months')
      .isSameOrAfter(moment(), 'day');

    if (dobDate.getTime() > dateNow.getTime() || isValidDate) {
      return { childMinAge: true };
    } else {
      return null;
    }
  } else {
    return null;
  }
}

export function maxChildAgeValid(control: FormControl): {
  [s: string]: boolean;
} {
  if (control.value && control.value.length === 10) {
    let dateNow = new Date();
    let dobDate = moment(control.value, 'DD/MM/YYYY').toDate();
    // The birth date - be sure to parse in the same format you're expecting:
    const dob = moment(control.value, 'DD/MM/YYYY');
    // The constant number of years you don't want to exceed
    const maxAge = 23;

    // Clone the dob (to not mutate it), increment by the years, and compare with day precision
    const isValidDate = dob
      .clone()
      .add(maxAge, 'years')
      .isSameOrBefore(moment(), 'day');

    if (dobDate.getTime() > dateNow.getTime() || isValidDate) {
      return { childMaxAge: true };
    } else {
      return null;
    }
  } else {
    return null;
  }
}

export function validPolicyStartDate(control: FormControl): {
  [s: string]: boolean;
} {
  if (control.value && control.value.length === 10) {
    const date = moment(control.value, 'DD/MM/YYYY');
    const sevenDays = moment(new Date()).subtract(7, 'd').format('DD/MM/YYYY');

    if (date.isBefore(moment(sevenDays, 'DD/MM/YYYY'))) {
      return { invalidPolicyDate: true };
    } else {
      return null;
    }
  } else {
    return null;
  }
}
