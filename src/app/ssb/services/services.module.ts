import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ErrorMessagePipe } from './pipes/error-message.pipe';
import { MaskNumberPipe } from './pipes/mask-number.pipe';

@NgModule({
  declarations: [ErrorMessagePipe, MaskNumberPipe],
  imports: [CommonModule],
  exports: [ErrorMessagePipe, MaskNumberPipe],
})
export class ServicesModule {}
