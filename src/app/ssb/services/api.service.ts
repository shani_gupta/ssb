import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable, BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  public url: string;
  environment: string = '';
  commonPath = '/ipdsv2/common/elements';
  commonCssPath = '/ipdsv2/common/common.css';

  public lincenseIntermediaryDataUrl = 'transact/pcIntermediaryData/';
  public logintag = true;
  public routerPathToAppend: string;
  public paymentUrl: string;
  public domain: string;
  public domainUrl: string;
  public productId: string = 'M100000000014';

  public email: string;
  public loginrole: string;
  public token: string;
  public capchacount: number = 0;
  public headers = {
    'in-auth-token': '',
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
  };
  public productLob = 'propertyandenergy';
  public profileDetails: any;
  public getBasicDetails: any = {};
  groupList: any = {};

  public loginTag: Observable<boolean>;
  public loginTagSubject: BehaviorSubject<boolean>;

  public host;
  searchUrl: any;
  loginUrl: string;
  result: string;
  productName: string;
  underWriterLevel: any;
  quoteData: any;
  getbasicdetails: any = {};
  product_group_name: string;
  constructor(
    private router: Router,
    private http: HttpClient,
    public spinner: NgxSpinnerService,
  ) {
    const { host } = window.location;
    console.log('HOST - ', host);

    if (host == 'localhost:4202') {
      this.environment = environment.environment;
    } else if (host == 'ipds1.cloware.in') {
      this.environment = 'DEV';
    } else if (host == 'uatipds1.cloware.in') {
      this.environment = 'UAT';
    }

    if (this.environment == 'LOCAL') {
      this.commonPath = 'https://uatipds1.cloware.in/ipdsv2/common/elements';
      this.commonCssPath =
        'https://uatipds1.cloware.in/ipdsv2/common/common.css';
      this.url = 'https://ipds2.cloware.in/api/v1/';
      this.domain = 'https://ipds2.cloware.in';
      this.domainUrl = 'https://ipds1.cloware.in/api/v1/';
      this.productId = 'M100000000014';
      this.routerPathToAppend = '';
      this.paymentUrl = '/ipdsv2/payment';
      this.product_group_name = 'QMS CAR';
    } else if (this.environment == 'DEV') {
      this.commonPath = 'https://uatipds1.cloware.in/ipdsv2/common/elements';
      this.commonCssPath =
        'https://uatipds1.cloware.in/ipdsv2/common/common.css';
      this.url = 'https://ipds1.cloware.in/api/v1/';
      this.domain = 'https://ipds2.cloware.in';
      this.domainUrl = 'https://ipds1.cloware.in/api/v1/';
      this.productId = 'M100000000014';
      this.routerPathToAppend = '';
      this.paymentUrl = '/ipdsv2/payment';
      this.product_group_name = 'QMS CAR';
      this.searchUrl = '/ipdsv2/login/#/search';
    } else if (this.environment == 'UAT') {
      this.commonPath = 'https://uatipds1.cloware.in/ipdsv2/common/elements';
      this.commonCssPath =
        'https://uatipds1.cloware.in/ipdsv2/common/common.css';
      this.url = 'https://uatipds1.cloware.in/api/v1/';
      this.domain = 'https://uatipds1.cloware.in';
      this.domainUrl = 'https://uatipds1.cloware.in/api/v1/';
      this.productId = 'M100000000014';
      this.routerPathToAppend = '';
      this.paymentUrl = '/ipdsv2/payment';
      this.product_group_name = 'CAR';
      this.searchUrl = 'https://uatipds1.cloware.in/ipdsv2/login/#/search';
    }
  }

  public get currentLoginTag(): boolean {
    return this.loginTagSubject.value;
  }

  public setCurrentLoginTag(value: boolean) {
    this.loginTagSubject.next(value);
  }

  async fetchProfileDetails(resolve) {
    await this.profile().subscribe((resp) => {
      if (resp.status === 0) {
        this.profileDetails = {
          //added ven for conver to proposal producer details
          is_admin: resp.data['is_admin'],
          is_broker: resp.data['is_broker'],
          is_UW: resp.data['is_underwriter'],
          level: resp.data['data']['level'] || '',
          email: resp.data['email'] || '',
          broker_id: resp.data['broker_id'] || '',
          producer_name: resp.data['first_name'],
          mobile_no: resp.data['mobile_no'],
          employee_id: resp.data['data']['employee_id'],
          broker_code: resp.data['broker_code'] || '',
          channel: resp.data['data']['channel'] || '',
          //added ven for conver to proposal producer details
        };
        sessionStorage.setItem(
          'profileDetails',
          JSON.stringify(this.profileDetails),
        );
        resolve();
      }
    });
  }

  async fetchProfile() {
    let res = await this.httpGetMethod(this.url + 'profile');
    console.log('res', res);
    if (res) {
      return res?.data;
    }
  }

  auth(data: any): Observable<any> {
    return this.http.post(this.url + 'auth', data);
  }

  profile(): Observable<any> {
    return this.http.get(this.url + 'profile', {
      headers: new HttpHeaders(this.headers),
    });
  }

  checkLogin() {
    if (
      (window.location.origin.includes('localhost') &&
        sessionStorage.getItem('in-auth-token') === '') ||
      sessionStorage.getItem('in-auth-token') === undefined ||
      sessionStorage.getItem('in-auth-token') === null
    ) {
      Swal.fire({
        icon: 'error',
        text: 'Token Not Found',
        allowOutsideClick: false,
      });
    } else {
      if (
        sessionStorage.getItem('in-auth-token') === '' ||
        sessionStorage.getItem('in-auth-token') === undefined ||
        sessionStorage.getItem('in-auth-token') === null
      ) {
        this.setCurrentLoginTag(true);
        if (this.environment != 'LOCAL') {
          window.location.href = this.result + this.loginUrl;
        }
      } else {
        this.setCurrentLoginTag(false);
        this.loginrole = sessionStorage.getItem('loginrole');
        this.headers['in-auth-token'] = sessionStorage.getItem('in-auth-token');
      }
    }
  }

  /****  Common HTTP GET Method****/
  async httpGetMethod(
    url: string,

    params = {},
    type = '',
    contentType = 'application/json',
    token = true,
    moduleName = '',
  ): Promise<any> {
    let options: any;
    options = await this.getHeaders(contentType, params);
    return await this.http
      .get(url, options)
      .toPromise()
      .then(async (response) => {
        let data = response['body'];
        if (data[`status`] == -114 || data[`status`] == -106) {
          Swal.fire({ text: 'Token is expired.', allowOutsideClick: false });
          sessionStorage.setItem('in-auth-token', '');
          this.setCurrentLoginTag(true);
        } else if (data.status == 0) {
          return data;
        } else if (
          data.status == -102 ||
          data.status == -103 ||
          data.status == -107
        ) {
          Swal.fire({ text: data[`txt`], allowOutsideClick: false });
        }
      });
  }

  getHeaders(contentType, params?) {
    let options;
    options = {
      headers: new HttpHeaders({
        'Content-Type': contentType,
        'in-auth-token': this.getCurrentToken(),
      }),
      observe: 'response',

      params,
    };
    return options;
  }

  getCurrentToken(): string {
    let token = '';
    token = sessionStorage.getItem('in-auth-token');
    return token;
  }

  /****  Common HTTP POST Method****/
  async httpPostMethod(
    url: string,
    parameter: any,
    contentType = 'application/json',
  ): Promise<any> {
    let options: any;
    options = this.get_headers(contentType);
    // console.log(options);
    return this.http
      .post(url, parameter, options)
      .toPromise()
      .then(async (response) => {
        let data = response['body'];
        if (data[`status`] == -114 || data[`status`] == -106) {
          Swal.fire({ text: 'Token is expired.', allowOutsideClick: false });
          sessionStorage.setItem('in-auth-token', '');
          this.logintag = true;
          if (this.environment == 'LOCAL')
            this.router.navigate([`${this.routerPathToAppend}/login`]);
          else window.location.href = this.result + this.loginUrl;
        } // Error Handling Here >
        else if (
          data.status == 0 ||
          data.status == -116 ||
          data.status == -104 ||
          data.status == -113 ||
          data.status == -107
        ) {
          this.readResponseHeader(data, response['headers']);
          return data;
        } else if (
          data.status == -102 ||
          data.status == -103 ||
          data.status == -107 ||
          data.status == -115
        ) {
          if (data[`txt`]?.includes('^rejected')) {
            return data;
          } else {
            Swal.fire({ text: data[`txt`], allowOutsideClick: false });
            return data;
          }
        }
      })
      .catch((err) => Promise.reject(err.error || 'Server error'));
  }

  /****  Common HTTP POST Method for Proposal BULK Upload****/
  async httpBulkPostMethod(
    url: string,
    parameter: any,
    contentType = 'application/json',
  ): Promise<any> {
    let options: any;
    options = this.get_headers(contentType);
    // console.log(options);
    return this.http
      .post(url, parameter, options)
      .toPromise()
      .then(async (response) => {
        let data = response['body'];
        if (data[`status`] == -114 || data[`status`] == -106) {
          Swal.fire({ text: 'Token is expired.', allowOutsideClick: false });
          sessionStorage.setItem('in-auth-token', '');
          this.logintag = true;
          if (this.environment == 'LOCAL')
            this.router.navigate([`${this.routerPathToAppend}/login`]);
          else window.location.href = this.result + this.loginUrl;
        } // Error Handling Here >
        else if (
          data.status == 0 ||
          data.status == -116 ||
          data.status == -104 ||
          data.status == -113 ||
          data.status == -107 ||
          data.status == -102 ||
          data.status == -103 ||
          data.status == -107 ||
          data.status == -115
        ) {
          this.readResponseHeader(data, response['headers']);
          return data;
        }
      })
      .catch((err) => Promise.reject(err.error || 'Server error'));
  }

  get_headers(contentType) {
    let options;
    options = {
      headers: new HttpHeaders({
        'Content-Type': contentType,
        'in-auth-token': this.getCurrentToken(),
      }),
      observe: 'response',
    };
    return options;
  }

  readResponseHeader(data, respHead) {
    // let token = '';
    // token = sessionStorage.getItem('in-auth-token');
  }

  /****  Common HTTP PDF Download Method****/
  async httpPdfDownload(url, policyID) {
    const httpOptions = {
      headers: new HttpHeaders({
        'in-auth-token': await this.getCurrentToken(),
      }),
      observe: 'response' as 'body',
      responseType: 'blob' as 'blob',
    };
    this.http
      .get(url, httpOptions)
      .toPromise()
      .then((response) => {
        let blob = new Blob([response['body']], { type: 'application/pdf' });
        let link = document.createElement('a');
        link.href = window.URL.createObjectURL(blob);
        link.download = policyID + '.pdf';
        link.click();
      })
      .catch((err) => Promise.reject(err.error || 'Server error'));
  }

  /*DELETE METHOD TO REMOVE DOCUMENT FROM PROPOSAL */
  async deleteMethod(url, params) {
    let options = this.getHeaderOption(params);
    let promise = new Promise((resolve, reject) => {
      this.http
        .delete(url, options)
        .toPromise()
        .then(
          (res) => {
            resolve(res);
          },
          (err) => {
            reject(err);
          },
        );
    });
    return promise;
  }

  /*METHOD TO SET HEADERS FOR FILE UPLOAD */
  getHeaderOption(webparams) {
    let options = {
      headers: new HttpHeaders({
        'in-auth-token': this.getCurrentToken(),
      }),
      params: webparams,
      observe: 'response' as 'response',
    };
    return options;
  }

  /*POST METHOD TO SAVE A DOCUMENT TO PROPOSAL */
  async post(url, body, params) {
    let promise = new Promise((resolve, reject) => {
      this.http
        .post(url, body, this.getHeaderOption(params))
        .toPromise()
        .then(
          (res) => {
            resolve(res);
          },
          (err) => {
            reject(err);
          },
        );
    });
    return promise;
  }

  async httpCsvDownload(url, fileName) {
    const httpOptions = {
      headers: new HttpHeaders({
        'in-auth-token': await this.getCurrentToken(),
      }),
      observe: 'response' as 'body',
      responseType: 'blob' as 'blob',
    };
    this.http
      .get(url, httpOptions)
      .toPromise()
      .then((response) => {
        let blob = new Blob([response['body']], { type: 'application/csv' });
        let link = document.createElement('a');
        link.href = window.URL.createObjectURL(blob);
        link.download = fileName + '.csv';
        link.click();
      })
      .catch((err) => Promise.reject(err.error || 'Server error'));
  }
}
