export const errorMessage = {
  required: 'Required *',
  minLength: 'Length must be at least ',
  maxLength: 'Length must not be greater than ',
  maxValue: 'Value can be maximum ',
  minValue: 'Value can be minimum ',
  email: 'Enter Valid Email',
  invalidPolicyDate: 'Date cannot be more than 7 days before.',
  invalidDobDate: 'Invalid DOB',
  invalidDate: 'Invalid Date',
  pattern: 'Invalid',
  invalidGstStateCode: "GSTIN doesn't matches with the state code",
  invalidPanGst: 'Pan number does not matches gstin number',
  invalidPincode: 'Enter Valid Pincode',
  minAge: 'Age is below than Minium Age limit 18 years',
  maxAge: 'Age is above than Maximum Age limit 70 years',
  childMinAge: 'Age is below than Minium Age limit 6 months',
  childMaxAge: 'Age is above than Maximum Age limit 23 years',
  invalidMinMaxLimit:
    'Minimum Age Limit Must Be Smaller than the Max Age Limit',
  isPastDate: 'Please Enter Current or Future Date',
};

export const errorMapperPattern = {
  gstinNumber: 'GSTIN number is invalid',
};

export const errorMapperRequired = {
  cityPincode: 'Pincode is required',
  occupation: 'Occupation is required',
  annualIncome: 'Annual income is required',
  self_dob: 'Date of birth is required',
  selfRiskClass: 'Risk class is required',
  spouse_dob: 'Date of birth is required',
};
