export const FRONTEND_DATE_FORMAT = 'DD/MM/YYYY';
export const BACKEND_DATE_FORMAT = 'MM/DD/YYYY';
export const INVALID_DATE = 'Invalid date';
export const VALID_FORM = 'VALID';
export const ERROR_MESSAGE = 'Something went wrong . Try Again';
export const BACKEND_TIME_FORMAT = 'YYYY-MM-DD HH:MM:SS';
export const Bancassurance = 'Bancassurance';
export const NAV_NEXT = 'Next';
export const CREATION_TIME_FORMAT = 'DD/MM/YYYY (h:mm A)';
export const FULLDTORIGINAL = 'DD-MM-YYYY-HH-MM-SS';
