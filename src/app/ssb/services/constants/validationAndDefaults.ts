export const validation = {
  gst: '^[0-9]{2}[a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[0-9]{1}[a-zA-Z]{1}[0-9a-zA-Z]{1}$',
  pan: '[A-Z]{5}[0-9]{4}[A-Z]{1}',
  email: '^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$',
};
export const defaults = {
  age: '000',
  plan: 'SELF',
  insuranceDetail: 'TATA AIG General Insurance',
};
