export const routes_name = {
  convertToProposal: 'convertToProposal',
  produceGeneralDetails: 'produceGeneralDetails',
  customerDetails: 'customerDetails',
  memberProposalDetails: 'memberProposalDetails',
  nomineeDetails: 'nomineeDetails',
  additionalPolicyDetails: 'additionalPolicyDetails',
  additionalDetails: 'additionalDetails',
  documentUpload: 'documentUpload',
};
