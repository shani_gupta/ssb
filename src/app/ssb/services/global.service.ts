import { Injectable } from '@angular/core';
import { EventManager } from '@angular/platform-browser';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class GlobalService {
  constructor(private eventManager: EventManager) {}

  gobackallapp() {
    window.location.href = environment.allapplication;
  }
  gobackallappPersonalAccident() {
    window.location.href = environment.allapplicationtravel;
  }

  formatName(evt: any) {
    var regex = new RegExp('^[a-zA-Z s]+$');
    var str = String.fromCharCode(!evt.charCode ? evt.which : evt.charCode);
    if (regex.test(str)) {
      return true;
    } else {
      evt.preventDefault();
      return false;
    }
  }

  // isNumberKey(evt: any) {
  //   if (
  //     (evt.key >= '0' && evt.key <= '9') ||
  //     evt.key == 'Backspace' ||
  //     evt.key == 'Delete' ||
  //     evt.key == 'ArrowLeft' ||
  //     evt.key == 'ArrowRight'
  //   ) {
  //     return true;
  //   }
  //   return false;
  // }
  restrictZero(event) {
    if (event.target.value.length == 0 && event.key == '0') {
      event.preventDefault();
    }
  }

  formatEmail(evt: any) {
    var regex = /[A-Z0-9a-z@\._-]/;
    var str = String.fromCharCode(!evt.charCode ? evt.which : evt.charCode);
    if (regex.test(str)) {
      return true;
    } else {
      evt.preventDefault();
      return false;
    }
  }

  checkValue(str, max) {
    if (str.charAt(0) !== '0' || str == '00') {
      var num = parseInt(str);
      if (isNaN(num) || num <= 0 || num > max) num = 1;
      str =
        num > parseInt(max.toString().charAt(0)) && num.toString().length == 1
          ? '0' + num
          : num.toString();
    }
    return str;
  }

  numericKeypress(event: any) {
    const pattern = /[0-9]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  numericslashKeypress(event: any) {
    const pattern = /[0-9/]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  isNumberKey(evt: any) {
    var dateData = evt.target.value;
    let temp = dateData;
    if (!(temp.length > 9)) {
      if (
        (evt.key >= '0' && evt.key <= '9') ||
        evt.key == 'Backspace' ||
        evt.key == 'Delete' ||
        evt.key == 'ArrowLeft' ||
        evt.key == 'ArrowRight'
      ) {
        return true;
      }
      return true;
    }
    return false;
  }

  preFormatDates(htmlElement) {
    this.eventManager.addEventListener(htmlElement, 'keydown', (e) => {
      var input = htmlElement.value;
      if (/[\-]/.test(input)) return;
      var key = e.keyCode || e.charCode;

      if (key == 8 || key == 46) return false;

      if (/\D\/$/.test(input)) input = input.substr(0, input.length - 1);
      var values = input.split('/').map(function (v) {
        return v.replace(/\D/g, '');
      });
      if (values[0]) values[0] = this.checkValue(values[0], 31);
      if (values[1]) values[1] = this.checkValue(values[1], 12);
      var output = values.map(function (v, i) {
        return v.length == 2 && i < 2 ? v + '/' : v;
      });
      htmlElement.value = output.join('').substr(0, 10);
    });
  }
}
