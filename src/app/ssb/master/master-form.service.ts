import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  isDatePast,
  maxAgeValid,
  minAgeValid,
} from 'src/app/ssb/services/utils/FormValidation';
import { VALID_FORM } from '../services/constants/constants';
import { ValidatorsService } from '../services/validators.service';

@Injectable({
  providedIn: 'root',
})
export class MasterFormService {
  masterForm: FormGroup;
  allowedExtensions = ['csv'];
  constructor(
    private _formBuilder: FormBuilder,
    private validatorService: ValidatorsService,
  ) {
    this.initializeForm();
  }

  private initializeForm(): void {
    this.masterForm = this._formBuilder.group({
      // Master Information
      masterInformationForm: this._formBuilder.group({
        underwriterRRGroup: this._formBuilder.group({
          ageLimitForSelfAndSpouse: this._formBuilder.group({
            maximumAge: [''],
            maximumAgeType: [''],
            minimumAge: [''],
            minimumAgeType: [''],
            offLimitConfiguration: [''],
          }),
          ageLimitForDependentChild: this._formBuilder.group({
            maximumAge: [''],
            maximumAgeType: [''],
            minimumAge: [''],
            minimumAgeType: [''],
            offLimitConfiguration: [''],
          }),
          maximumBaseSiLimit: this._formBuilder.group({
            maxLimit: [''],
            offLimitConfiguration: [''],
          }),
          medicalHistory: this._formBuilder.group({
            offLimitConfiguration: [''],
          }),
        }),
        sumInsuredLimit: ['50,000,00', Validators.max(50000000)],
        sortAndFilter: this._formBuilder.group({
          channel: [''],
          location: [''],
        }),
        addItem: [null],
        addSumInsuredLimit: this._formBuilder.group({
          channel: [null, Validators.required],
          producerCode: [null],
          location: [null],
          applicableOnChildProducers: [null, Validators.required],
          effectiveStartDate: ['', [Validators.required, isDatePast]],
          effectiveEndDate: ['', [Validators.required, isDatePast]],
          minAgeForSelf: [18, [Validators.required, minAgeValid, maxAgeValid]],
          maxAgeForSelf: [70, [Validators.required, minAgeValid, maxAgeValid]],
          sumInsuredLimit: [
            '',
            [Validators.required, Validators.max(50000000)],
          ],
        }),
        riskClass: this._formBuilder.group({
          riskClass1: [null],
          riskClass2: [null],
          riskClass3: [null],
          riskClass4: [null],
          riskClass5: [null],
          riskClassSpouse1: [null],
          riskClassSpouse2: [null],
          riskClassSpouse3: [null],
          riskClassSpouse4: [null],
          riskClassSpouse5: [null],
          riskClassChild1: [null],
          riskClassInlaw1: [null],
          riskClassInlaw2: [null],
          riskClassInlaw3: [null],
          riskClassInlaw4: [null],
          riskClassInlaw5: [null],
        }),
      }),

      // Bulk Upload Form
      bulkUploadForm: this._formBuilder.group({
        document: [
          '',
          this.validatorService.fileExtensions(this.allowedExtensions),
        ],
        documentName: [''],
        documentType: [''],
        documentSize: [''],
      }),

      // View Bulk Upload Form
      viewBulkUploadStatusForm: this._formBuilder.group({
        searchBatchID: [''],
      }),
    });
  }

  get masterInformationForm() {
    return this.masterForm?.get('masterInformationForm') as FormGroup;
  }

  get underwriterRRGroup() {
    return this.masterInformationForm?.get('underwriterRRGroup') as FormGroup;
  }

  get bulkUploadForm() {
    return this.masterForm?.get('bulkUploadForm') as FormGroup;
  }

  get viewBulkUploadStatusForm() {
    return this.masterForm?.get('viewBulkUploadStatusForm') as FormGroup;
  }

  get isMasterInformationFormValid() {
    return (
      this.masterInformationForm.controls.underwriterRRGroup.status ===
      VALID_FORM
    );
  }

  get isaddSumInsuredLimitFormValid() {
    return (
      this.masterInformationForm.controls.addSumInsuredLimit.status ===
      VALID_FORM
    );
  }
}
