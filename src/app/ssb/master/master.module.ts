import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MasterRoutingModule } from './master-routing.module';
import { MasterInformationComponent } from './master-information/master-information.component';
import { BulkUploadComponent } from './bulk-upload/bulk-upload.component';
import { ViewBulkUploadStatusComponent } from './view-bulk-upload-status/view-bulk-upload-status.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrModule } from 'ngx-toastr';
import { TranslateModule } from '@ngx-translate/core';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

@NgModule({
  declarations: [
    MasterInformationComponent,
    BulkUploadComponent,
    ViewBulkUploadStatusComponent,
  ],
  imports: [
    CommonModule,
    MasterRoutingModule,
    NgSelectModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    NgxPaginationModule,
    ToastrModule.forRoot({
      timeOut: 10000,
      preventDuplicates: true,
    }),
    TranslateModule,
    NgMultiSelectDropDownModule,
  ],
})
export class MasterModule {}
