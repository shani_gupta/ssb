import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewBulkUploadStatusComponent } from './view-bulk-upload-status.component';

describe('ViewBulkUploadStatusComponent', () => {
  let component: ViewBulkUploadStatusComponent;
  let fixture: ComponentFixture<ViewBulkUploadStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ViewBulkUploadStatusComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewBulkUploadStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
