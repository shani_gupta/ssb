import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { MasterFormService } from '../master-form.service';
// import { MasterService } from '../master.service';

@Component({
  selector: 'app-view-bulk-upload-status',
  templateUrl: './view-bulk-upload-status.component.html',
  styleUrls: ['./view-bulk-upload-status.component.scss'],
})
export class ViewBulkUploadStatusComponent implements OnInit {
  page: number = 1;
  bulkUploadStatus;
  searchText = '';
  viewBulkUploadStatusForm: FormGroup;

  constructor(
    private _router: Router,
    private masterFormService: MasterFormService, // private masterService: MasterService
  ) {}

  ngOnInit(): void {
    this.viewBulkUploadStatusForm =
      this.masterFormService.viewBulkUploadStatusForm;
    this.getBulkUploadStatus();
  }

  /* Get Bulk Upload Status Start */
  async getBulkUploadStatus() {
    // let res = await this.masterService.getBulkUploadStatus();
    // if (res) {
    //   this.bulkUploadStatus = res;
    // }
  }
  /* Get Bulk Upload Status End */

  /* Download Bulk Upload Report Start */
  downloadBulkUploadReport(id) {
    // this.masterService.downloadLogReport(id);
  }
  /* Download Bulk Upload Report End */

  /* Search Batch ID Start */
  async search() {
    // let res;
    // if (this.searchText && this.searchText.length > 0) {
    //   res = await this.masterService.getBulkUploadStatus();
    //   if (res) {
    //     res = res.filter((item) => {
    //       return item.batchId === Number(this.searchText);
    //     });
    //   }
    // } else {
    //   res = await this.masterService.getBulkUploadStatus();
    // }
    // this.bulkUploadStatus = res;
  }
  /* Search Batch ID End */

  routeBack() {
    this._router.navigate(['accident-guard-plus/master/bulkUpload']);
  }
}
