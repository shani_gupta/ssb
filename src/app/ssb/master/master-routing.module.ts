import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BulkUploadComponent } from './bulk-upload/bulk-upload.component';
import { MasterInformationComponent } from './master-information/master-information.component';
import { ViewBulkUploadStatusComponent } from './view-bulk-upload-status/view-bulk-upload-status.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: MasterInformationComponent,
  },
  {
    path: 'masterInformation',
    component: MasterInformationComponent,
  },
  {
    path: 'bulkUpload',
    component: BulkUploadComponent,
  },
  {
    path: 'viewBulkUploadStatus',
    pathMatch: 'full',
    component: ViewBulkUploadStatusComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MasterRoutingModule {}
