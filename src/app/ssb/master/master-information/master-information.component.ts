import {
  Component,
  ElementRef,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormGroup,
} from '@angular/forms';
import { EventManager } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { checkFormControl } from 'src/app/ssb/services/utils/FormValidation';
import { MasterFormService } from '../master-form.service';
import { GlobalService } from '../../services/global.service';
import { months } from '../../services/constants/months';

@Component({
  selector: 'app-master-information',
  templateUrl: './master-information.component.html',
  styleUrls: ['./master-information.component.scss'],
})
export class MasterInformationComponent implements OnInit {
  masterInformationForm: FormGroup;
  page: number = 1;
  updateModalRef: BsModalRef;
  selectedPageTitle: string = '';
  selectedBreadCrum: string = '';
  selectedTopNav: string = '';
  uwReferralRuleSelected: Boolean = false;
  sumInsuredLimit: Boolean = false;
  riskClass: Boolean = false;
  displayUWReferralForm: Boolean = false;
  UWReferralRuleForm: FormGroup;
  isMobileView: Boolean = false;
  dropdownList = [];
  dropdownListChannel = [];
  selectedItems = [];
  riskClassArr = [
    'Risk Class I',
    'Risk Class II',
    'Risk Class III',
    'Risk Class IV',
    'Risk Class V',
  ];
  selectedItemsChannel = [];
  config: any = {
    backdrop: 'static',
    keyboard: false,
    class: 'theme-modal',
  };
  dropDownArr = [
    {
      id: 1,
      type: 'Add Manualy',
    },
    {
      id: 2,
      type: 'Via Bulk Upload',
    },
  ];
  array = [1, 2, 3, 4, 5];
  editSiLimitModalRef: BsModalRef;
  sortFilterModalRef: BsModalRef;
  dropdownSettings: IDropdownSettings = {};
  searchDropDownSettings: IDropdownSettings = {};
  channelDropdown = [];
  locationDropdown = [];
  producerCodesDropdown = [];
  boolDropdown = [
    { id: 1, name: 'Yes' },
    { id: 2, name: 'No' },
  ];
  producerCodesModalRef: BsModalRef;
  dataArr = [];
  locationArr = [];
  locationCodesModalRef: BsModalRef;
  sumInsuredLimitModalRef: BsModalRef;
  offLimitArr: string[] = ['Send to UW', 'Retrict user', 'None'];
  addSILimit: Boolean = false;
  editSILimit: Boolean = false;
  ageSiLimitData = [];
  skillsForm: FormGroup;
  @ViewChild('effectiveStartDate') effectiveStartDate: ElementRef;
  @ViewChild('effectiveEndDate') effectiveEndDate: ElementRef;

  constructor(
    private _router: Router,
    private _formBuilder: FormBuilder,
    private translate: TranslateService,
    private eventManager: EventManager,
    private modalService: BsModalService,
    private masterFormService: MasterFormService,
    public helper: GlobalService, // private masterService: MasterService, // private product: ProductService
  ) {}

  ngOnInit(): void {
    this.initializePageTitles();
    this.masterInformationForm = this.masterFormService.masterInformationForm;
    (async () => {
      await this.fetchDataForDropDowns();
    })().catch((err) => {
      console.error(err);
    });
    console.log('FORM INIT', this.masterInformationForm.value);

    this.topNavChanged('uwReferralRuleSelected');
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'name',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 2,
      allowSearchFilter: false,
    };
    this.searchDropDownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'name',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 2,
      allowSearchFilter: true,
    };
  }

  initializePageTitles() {
    this.selectedPageTitle = this.translate.instant('masterInformation.title');
    this.selectedBreadCrum = this.translate.instant('masterInformation.title');
    this.selectedTopNav = this.translate.instant(
      'masterInformation.underwriterRefferalRules.nav',
    );
    this.uwReferralRuleSelected = true;
    this.displayUWReferralForm = true;
  }

  /* Fetch Data for Drop Down Start */
  private async fetchDataForDropDowns() {
    //this.occupationArr = await this.product.getOccupationMaster();
    // this.offLimitArr = await this.product.getUWAction();
    // await this.getAgeSumInsuredLimit();
    // await this.getLocation();
    // await this.getProducerCode();
    // await this.getChannel();
  }
  /* Fetch Data for Drop Down End */

  /* Get Location Data Start */
  async getLocation() {
    // let res = await this.product.getLocation();
    // res.map((ele, idx) => {
    //   let obj = {
    //     id: idx + 1,
    //     name: ele?.location_name,
    //   };
    //   this.locationDropdown.push(obj?.name);
    // });
  }
  /* Get Location Data End */

  /* Get Producer Code Start */
  async getProducerCode() {
    // let res = await this.product.getProducer();
    // res.map((ele, idx) => {
    //   let obj = {
    //     id: idx + 1,
    //     name: ele?.producer_code,
    //   };
    //   this.producerCodesDropdown.push(obj?.name);
    // });
  }
  /* Get Producer Code End */

  /* Get Channel Start */
  async getChannel() {
    // let res = await this.product.getChannel();
    // res.map((ele, idx) => {
    //   let obj = {
    //     id: idx + 1,
    //     name: ele?.channel_type,
    //   };
    //   this.channelDropdown.push(obj?.name);
    // });
  }
  /* Get Channel End */

  /* To get view of Age and Suminsured Limit Start */
  async getAgeSumInsuredLimit() {
    // let res = await this.masterService.getAgeSumInsuredLimit();
    // console.log('res age:::', res);
    // res.map((ele) => {
    //   let obj = {
    //     channel: ele?.channel,
    //     child_producer: ele?.child_producer,
    //     effective_end_date: moment(
    //       ele?.effective_end_date,
    //       'YYYY-MM-DD'
    //     ).format('DD/MM/YYYY'),
    //     effective_start_date: moment(
    //       ele?.effective_start_date,
    //       'YYYY-MM-DD'
    //     ).format('DD/MM/YYYY'),
    //     location: ele?.location,
    //     max_age_Adult: ele?.max_age_Adult,
    //     min_age_Adult: ele?.min_age_Adult,
    //     producer_code: ele?.producer_code,
    //     producer_silimit: ele?.producer_silimit,
    //     reference_id: ele?.reference_id,
    //   };
    //   this.ageSiLimitData.push(obj);
    // });
  }
  /* To get view of Age and Suminsured Limit End */

  /* Set Age and Sum Insured Data */
  setAgeSiLimitArr() {
    let formData = this.masterInformationForm.controls.sortAndFilter.value;
    let channel = formData?.channel;
    let location = formData?.location;
    if (channel.length > 0 && location.length > 0) {
      this.ageSiLimitData = this.ageSiLimitData.filter((data) => {
        return (
          channel.includes(data?.channel) &&
          location.some((l) => data?.location.includes(l))
        );
      });
    } else if (channel.length > 0 && location.length === 0) {
      this.ageSiLimitData = this.ageSiLimitData.filter((data) => {
        return channel.includes(data?.channel);
      });
    } else if (channel.length === 0 && location.length > 0) {
      this.ageSiLimitData = this.ageSiLimitData.filter((data) => {
        return location.some((l) => data?.location.includes(l));
      });
    }
    this.clearFilter();
  }
  /* Set Age and Sum Insured Data */

  /* Clear Filter Start */
  clearFilter() {
    this.masterInformationForm.get('sortAndFilter').reset();
  }
  /* Clear Filter End */

  topNavChanged(nav) {
    switch (nav) {
      case 'uwReferralRules': {
        this.initializePageTitles();
        this.uwReferralRuleSelected = true;
        this.sumInsuredLimit = false;
        // this.displayUWReferralForm = false;
        this.riskClass = false;
        break;
      }
      case 'sumInsuredLimit': {
        this.selectedPageTitle = this.translate.instant(
          'masterInformation.sumInsured.title',
        );
        this.selectedBreadCrum = this.translate.instant(
          'masterInformation.sumInsured.title',
        );
        this.selectedTopNav = this.translate.instant(
          'masterInformation.sumInsured.nav',
        );
        this.uwReferralRuleSelected = false;
        this.sumInsuredLimit = true;
        // this.displayUWReferralForm = false;
        this.riskClass = false;
        break;
      }
      case 'riskClass': {
        this.selectedPageTitle = this.translate.instant(
          'masterInformation.riskClass.title',
        );
        this.selectedBreadCrum = this.translate.instant(
          'masterInformation.riskClass.title',
        );
        this.selectedTopNav = this.translate.instant(
          'masterInformation.riskClass.nav',
        );
        this.riskClass = true;
        this.uwReferralRuleSelected = false;
        // this.displayUWReferralForm = false;
        this.sumInsuredLimit = false;
        break;
      }
    }
  }

  formValue() {
    console.log(this.masterInformationForm.value);
  }

  isNumberKey(evt: any) {
    if (
      (evt.key >= '0' && evt.key <= '9') ||
      evt.key == 'Backspace' ||
      evt.key == 'Delete' ||
      evt.key == 'ArrowLeft' ||
      evt.key == 'ArrowRight'
    ) {
      return true;
    }
    return false;
  }

  openUpdateModal(update_template: TemplateRef<any>) {
    if (this.masterFormService.isMasterInformationFormValid) {
      this.updateModalRef = this.modalService.show(
        update_template,
        this.config,
      );
    }
  }

  openSetSILimitModal(edit_limit_template: TemplateRef<any>) {
    this.editSiLimitModalRef = this.modalService.show(
      edit_limit_template,
      this.config,
    );
  }

  openSortFilterModal(sort_filter_template: TemplateRef<any>) {
    this.sortFilterModalRef = this.modalService.show(
      sort_filter_template,
      this.config,
    );
  }

  onItemSelect(item: any) {
    console.log(item);
  }
  onSelectAll(items: any) {
    console.log(items);
  }

  openProducerCodesModal(producer_codes_template: TemplateRef<any>, item) {
    this.producerCodesModalRef = this.modalService.show(
      producer_codes_template,
      this.config,
    );
    this.dataArr = [];
    this.dataArr.push(item?.producer_code);
  }

  openLocationCodesModal(location_codes_template: TemplateRef<any>, item) {
    this.locationCodesModalRef = this.modalService.show(
      location_codes_template,
      this.config,
    );
    this.locationArr = [];
    this.locationArr.push(item?.location);
  }

  dropdownChange(sumInsured_limit_template: TemplateRef<any>) {
    if (this.masterInformationForm.get('addItem').value == 1) {
      this.openSumInsuredLimitModal(sumInsured_limit_template, 'add', 0);
    } else if (this.masterInformationForm.get('addItem').value == 2) {
      this.routeToBulkUpload();
    }
  }

  openSumInsuredLimitModal(
    sumInsured_limit_template: TemplateRef<any>,
    modalType,
    item,
  ) {
    this.sumInsuredLimitModalRef = this.modalService.show(
      sumInsured_limit_template,
      this.config,
    );
    this.formatDates();
    switch (modalType) {
      case 'add': {
        this.addSILimit = true;
        this.editSILimit = false;
        break;
      }
      case 'edit': {
        this.addSILimit = true;
        this.editSILimit = true;
        if (item) {
          this.setSumInsuredLimitModal(item);
        }
      }
    }
  }

  /* Edit setSumInsuredLimitModal Start */
  setSumInsuredLimitModal(item) {
    console.log('res age itm:::', item);
    this.masterInformationForm
      ?.get('addSumInsuredLimit')
      ?.get('channel')
      .setValue(item?.channel);
    this.masterInformationForm
      ?.get('addSumInsuredLimit')
      ?.get('producerCode')
      .setValue(item?.producer_code);
    this.masterInformationForm
      ?.get('addSumInsuredLimit')
      ?.get('location')
      .setValue(item?.location);
    this.masterInformationForm
      ?.get('addSumInsuredLimit')
      ?.get('applicableOnChildProducers')
      .setValue(item?.child_producer === 'Yes' ? 1 : 2);
    this.masterInformationForm
      ?.get('addSumInsuredLimit')
      ?.get('effectiveStartDate')
      .setValue(item?.effective_start_date);
    this.masterInformationForm
      ?.get('addSumInsuredLimit')
      ?.get('effectiveEndDate')
      .setValue(item?.effective_end_date);
    this.masterInformationForm
      ?.get('addSumInsuredLimit')
      ?.get('minAgeForSelf')
      .setValue(item?.min_age_Adult);
    this.masterInformationForm
      ?.get('addSumInsuredLimit')
      ?.get('maxAgeForSelf')
      .setValue(item?.max_age_Adult);
    this.masterInformationForm
      ?.get('addSumInsuredLimit')
      ?.get('sumInsuredLimit')
      .setValue(item?.producer_silimit);
  }
  /* Edit setSumInsuredLimitModal End */

  /* Form Reset Start */
  formReset() {
    this.masterInformationForm?.get('addSumInsuredLimit').reset();
  }
  /* Form Reset End */

  /* Check Max and Min Age Start */
  minMaxAgeCompare() {
    let minAge = parseInt(
      this.masterInformationForm?.get('addSumInsuredLimit')?.value
        ?.minAgeForSelf,
    );
    let maxAge = parseInt(
      this.masterInformationForm?.get('addSumInsuredLimit')?.value
        ?.maxAgeForSelf,
    );
    console.log('minmax age:::::', minAge, maxAge, minAge > maxAge);
    if (minAge > maxAge) {
      console.log('min enter');
      this.masterInformationForm
        ?.get('addSumInsuredLimit')
        ?.get('minAgeForSelf')
        ?.setErrors({ invalidMinMaxLimit: true });
      this.masterInformationForm
        ?.get('addSumInsuredLimit')
        ?.get('minAgeForSelf')
        ?.markAsTouched();
      console.log(
        'min master form',
        this.masterInformationForm?.get('addSumInsuredLimit'),
      );
    }
    // else {
    //   this.masterInformationForm
    //     ?.get('addSumInsuredLimit')
    //     ?.get('minAgeForSelf')
    //     ?.setErrors({ invalidMinMaxLimit: false });
    // }
  }
  /* Check Max and Min Age End */

  saveSiLimitBtn(update_template: TemplateRef<any>) {
    this.sumInsuredLimitModalRef.hide();
    this.openUpdateModal(update_template);
  }

  routeToBulkUpload() {
    this._router.navigate(['accident-guard-plus/master/bulkUpload']);
  }

  formatDates() {
    setTimeout(() => {
      this.helper.preFormatDates(document.getElementById('effectiveStartDate'));
      this.helper.preFormatDates(document.getElementById('effectiveEndDate'));
    }, 100);
  }

  checkValue(str, max) {
    if (str.charAt(0) !== '0' || str == '00') {
      var num = parseInt(str);
      if (isNaN(num) || num <= 0 || num > max) num = 1;
      str =
        num > parseInt(max.toString().charAt(0)) && num.toString().length == 1
          ? '0' + num
          : num.toString();
    }
    return str;
  }

  formatLikeExcel(dateControl) {
    var values = this.masterInformationForm
      .get('addSumInsuredLimit')
      .get(dateControl).value;

    var arr = values.split('-');
    // console.log("parsed value",arr,arr.length,arr[1],parseInt(arr[1]));
    if (arr.length > 1) {
      arr[1] = parseInt(arr[1])
        ? parseInt(arr[1])
        : months[arr[1].toLocaleLowerCase()];
      arr[2] = parseInt(arr[2]) ? parseInt(arr[2]) : '';
      var finalValue = arr.join('/');
      // htmlElement.value = finalValue;
      this.masterInformationForm
        .get('addSumInsuredLimit')
        .get(dateControl)
        .setValue(finalValue);
    } else {
      var arr = values.split('/');
      if (arr.length > 1) {
        arr[1] = parseInt(arr[1])
          ? parseInt(arr[1])
          : months[arr[1].toLocaleLowerCase()];
        arr[2] = parseInt(arr[2]) ? parseInt(arr[2]) : '';
        var finalValue = arr.join('/');
        this.masterInformationForm
          .get('addSumInsuredLimit')
          .get(dateControl)
          .setValue(finalValue);
      }
    }
  }

  /* Underwritter Refferal COnfiguration Update */
  async uwConfigUpdate(update_template: TemplateRef<any>) {
    // this.masterInformationForm?.get('underwriterRRGroup').markAllAsTouched();
    // if (this.masterFormService.isMasterInformationFormValid) {
    //   let res = await this.masterService.uwReferralConfig(
    //     this.masterInformationForm.controls.underwriterRRGroup.value
    //   );
    //   if (res['status'] === 0) {
    //     this.updateModalRef = this.modalService.show(
    //       update_template,
    //       this.config
    //     );
    //   }
    // }
  }
  /* Underwritter Refferal COnfiguration Update */

  /* Age and Sum Insured Limit Update */
  async ageSumInsuredUpdate(update_template: TemplateRef<any>) {
    // this.masterInformationForm?.get('addSumInsuredLimit').markAllAsTouched();
    // if (this.masterFormService.isaddSumInsuredLimitFormValid) {
    //   let res = await this.masterService.ageSumInsuredConfig(
    //     this.masterInformationForm.controls.addSumInsuredLimit.value,
    //     this.masterInformationForm.controls.sumInsuredLimit.value
    //   );
    //   if (res?.['status'] === 0) {
    //     this.sumInsuredLimitModalRef.hide();
    //     this.openUpdateModal(update_template);
    //     this.formReset();
    //   }
    // }
  }
  /* Age and Sum Insured Limit Update */

  /* Download Master Data Start */
  async downloadMasterData() {
    // await this.masterService.downloadMasterData();
  }
  /* Download Master Data End */

  public getErrorMessage(control: AbstractControl): string {
    return checkFormControl(control);
  }
}
