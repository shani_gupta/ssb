import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterInformationComponent } from './master-information.component';

describe('MasterInformationComponent', () => {
  let component: MasterInformationComponent;
  let fixture: ComponentFixture<MasterInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MasterInformationComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
