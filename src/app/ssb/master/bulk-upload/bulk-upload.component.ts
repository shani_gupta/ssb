import {
  ChangeDetectorRef,
  Component,
  OnInit,
  TemplateRef,
} from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ToastrService } from 'ngx-toastr';
import { checkFormControl } from 'src/app/ssb/services/utils/FormValidation';
import { MasterFormService } from '../master-form.service';
// import { MasterService } from '../master.service';

@Component({
  selector: 'app-bulk-upload',
  templateUrl: './bulk-upload.component.html',
  styleUrls: ['./bulk-upload.component.scss'],
})
export class BulkUploadComponent implements OnInit {
  bulkUploadForm: FormGroup;
  uploadModalRef: BsModalRef;
  config: any = {
    backdrop: 'static',
    keyboard: false,
    class: 'theme-modal',
  };
  uploadId = '';

  constructor(
    private _router: Router,
    private _formBuilder: FormBuilder,
    private cd: ChangeDetectorRef,
    private modalService: BsModalService,
    private masterFormService: MasterFormService,
    private toaster: ToastrService, // private masterService: MasterService
  ) {}

  ngOnInit(): void {
    this.bulkUploadForm = this.masterFormService.bulkUploadForm;
  }

  onFileChange(event) {
    let reader = new FileReader();

    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      this.setFormFileName(file);
      reader.readAsDataURL(file);

      reader.onload = () => {
        this.cd.markForCheck();
      };
    }
  }

  setFormFileName(file) {
    this.bulkUploadForm.get('documentName').setValue(file.name);
    this.bulkUploadForm.get('document').setValue(file);
  }

  async createFormAndSubmitDoc(param) {
    // if (this.bulkUploadForm.status === 'VALID') {
    //   let formData = new FormData();
    //   formData.append('file', param.document);
    //   let res = await this.masterService.bulkUpload(formData);
    //   if (res) {
    //     this.uploadId = res.txt;
    //     await this.masterService.publish();
    //     this.bulkUploadForm = this.masterFormService.bulkUploadForm;
    //     await this.masterService.fetchSumInsuredMaster();
    //     return 1;
    //   } else {
    //     this.bulkUploadForm = this.masterFormService.bulkUploadForm;
    //     return 0;
    //   }
    // }
  }

  removeDocFile() {
    this.bulkUploadForm.get('documentName').setValue('');
  }

  routeBack() {
    this._router.navigate(['accident-guard-plus/master']);
  }

  async openUploadModal(upload_template: TemplateRef<any>) {
    if (
      this.bulkUploadForm.value.document &&
      this.bulkUploadForm.status === 'VALID'
    ) {
      let res = await this.createFormAndSubmitDoc(this.bulkUploadForm.value);
      // if (res && res === 1) {
      //   this.uploadModalRef = this.modalService.show(
      //     upload_template,
      //     this.config
      //   );
      // }
    }
  }

  routeToViewBulkUploadStatus() {
    this._router.navigate(['accident-guard-plus/master/viewBulkUploadStatus']);
  }

  public getErrorMessage(control: AbstractControl): string {
    return checkFormControl(control);
  }
}
