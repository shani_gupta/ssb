import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UwProposalListingComponent } from './uw-proposal-listing.component';

describe('UwProposalListingComponent', () => {
  let component: UwProposalListingComponent;
  let fixture: ComponentFixture<UwProposalListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UwProposalListingComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UwProposalListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
