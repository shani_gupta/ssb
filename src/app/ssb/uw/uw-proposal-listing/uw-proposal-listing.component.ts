import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { WhiteSpaceValidator } from 'src/app/directives/white-space.validator';
import { GlobalService } from '../../services/global.service';

@Component({
  selector: 'app-uw-proposal-listing',
  templateUrl: './uw-proposal-listing.component.html',
  styleUrls: ['./uw-proposal-listing.component.scss'],
})
export class UwProposalListingComponent implements OnInit {
  model: any = {};
  config: NgbModalOptions = {
    backdrop: 'static',
    keyboard: false,
  };
  gridItem: boolean = true;
  listItem: boolean = false;
  itemsPerPageGrid: string = '12';
  itemsPerPageList: string = '30';
  page: number = 1;
  filter_ModalRef: BsModalRef;
  mobileOptionsDropdown: boolean = false;
  sortFilterForm: FormGroup;
  isStartDateValid = true;
  isEndDateValid = true;
  dropdownSettings2 = {};
  getQuoteList = [
    {
      insured_name: 'Mr. Rajesh Arora',
      enabled: false,
      quote_id: 'Q299838990',
      quote_no: '765765689',
      quote_date: '26/01/2009',
      quote_status: 'Proposal Converted',
      instalment: 'Yes',
      premium_value: '42313.07',
    },
    {
      insured_name: 'Mr. Rajesh Arora',
      enabled: false,
      quote_id: 'Q299838990',
      quote_no: '765765689',
      quote_date: '26/01/2009',
      quote_status: 'Refered to UW',
      instalment: 'Yes',
      premium_value: '42313.07',
    },
    {
      insured_name: 'Mr. Rajesh Arora',
      enabled: false,
      quote_id: 'Q299838990',
      quote_no: '765765689',
      quote_date: '26/01/2009',
      quote_status: 'Rejected to Refund Initiated',
      instalment: 'Yes',
      premium_value: '42313.07',
    },
    {
      insured_name: 'Mr. Rajesh Arora',
      enabled: false,
      quote_id: 'Q299838990',
      quote_no: '765765689',
      quote_date: '26/01/2009',
      quote_status: 'Add info Discrepancy',
      instalment: 'Yes',
      premium_value: '42313.07',
    },
    {
      insured_name: 'Mr. Rajesh Arora',
      enabled: false,
      quote_id: 'Q299838990',
      quote_no: '765765689',
      quote_date: '26/01/2009',
      quote_status: 'Rejeced - Refunded',
      instalment: 'Yes',
      premium_value: '42313.07',
    },
  ];
  months: Object = {
    jan: '01',
    feb: '02',
    mar: '03',
    apr: '04',
    may: '05',
    jun: '06',
    jul: '07',
    aug: '08',
    sep: '09',
    oct: '10',
    nov: '11',
    dec: '12',
  };

  dropdownList = [];
  selectedItems = [];

  constructor(
    private router: Router,
    private modalService: BsModalService,
    private formBuilder: FormBuilder,
    public global: GlobalService,
  ) {}

  ngOnInit(): void {
    this.config['class'] = ' theme-modal';
    this.mobileOptionsDropdown = window.innerWidth < 768;
    window.onresize = () => {
      this.mobileOptionsDropdown = window.innerWidth < 768;
    };

    this.dropdownSettings2 = {
      singleSelection: false,
      idField: 'channel_type',
      textField: 'channel_type',
      itemsShowLimit: 1,
      classes: 'myclass custom-class',
    };
  }

  onItemSelect(item: any) {
    console.log(item);
  }
  onSelectAll(items: any) {
    console.log(items);
  }

  back() {
    this.router.navigate(['']);
  }

  newQuote() {
    this.router.navigate(['']);
  }

  closeDropMenu() {
    this.getQuoteList.forEach((element) => {
      element.enabled = false;
    });
  }
  onEventStopPropogation(event) {
    event.stopPropagation();
  }

  initForm() {
    this.sortFilterForm = this.formBuilder.group({
      startDate: ['', WhiteSpaceValidator.restrictWhiteSpaces],
      endDate: ['', WhiteSpaceValidator.restrictWhiteSpaces],
      sort: [null, WhiteSpaceValidator.restrictWhiteSpaces],
      proposalStatus: [null, WhiteSpaceValidator.restrictWhiteSpaces],
      sumInsuredValueFrom: [0, WhiteSpaceValidator.restrictWhiteSpaces],
      sumInsuredValueTo: [0, WhiteSpaceValidator.restrictWhiteSpaces],
      tenure: [null, WhiteSpaceValidator.restrictWhiteSpaces],
      select_product: [null, WhiteSpaceValidator.restrictWhiteSpaces],
    });
  }

  getTenure(event) {}

  openFilterModel(sortfilter_template: TemplateRef<any>, el: HTMLElement) {
    el.scrollIntoView({ behavior: 'smooth' }); //popup not open issue bugid 30759
    this.initForm();
    this.filter_ModalRef = this.modalService.show(
      sortfilter_template,
      this.config,
    );
  }

  gotoReview(status) {
    if (status === 'Add info Discrepancy') {
      this.router.navigate(['ssb/UwDiscrepancy']);
    } else this.router.navigate(['ssb/UwReviewProposal']);
  }

  getpolicies(type) {}

  grid() {
    this.gridItem = true;
    this.listItem = false;
  }
  list() {
    this.listItem = true;
    this.gridItem = false;
  }

  loadData() {}

  applyFilter() {}

  clearFilter() {}

  formatLikeExcel(val) {
    var values;
    if (val === 'startDate') {
      values = this.sortFilterForm?.controls?.startDate?.value;
    } else if (val === 'endDate') {
      values = this.sortFilterForm?.controls?.endDate?.value;
    }
    //var values= this.sortFilterForm.controls.startDate.value
    var arr = values.split('-');
    if (arr.length > 1) {
      arr[1] = parseInt(arr[1])
        ? parseInt(arr[1])
        : this.months[arr[1].toLocaleLowerCase()];
      arr[2] = parseInt(arr[2]) ? parseInt(arr[2]) : '';
      var finalValue = arr.join('/');
      if (val === 'startDate') {
        this.sortFilterForm.controls.startDate.setValue(finalValue);
      } else if (val === 'endDate') {
        this.sortFilterForm.controls.endDate.setValue(finalValue);
      }
    }
  }

  isNumberKey(evt: any) {
    let temp = evt?.target?.value;
    if (!(temp.length > 9)) {
      if (
        (evt.key >= '0' && evt.key <= '9') ||
        evt.key == 'Backspace' ||
        evt.key == 'Delete' ||
        evt.key == 'ArrowLeft' ||
        evt.key == 'ArrowRight'
      ) {
        return true;
      }
    }
    return false;
  }

  dateValidation(date: any): boolean {
    const temp = date.split('/');
    const d = new Date(temp[1] + '/' + temp[0] + '/' + temp[2]);
    const isValidDate =
      d &&
      d.getMonth() + 1 == temp[1] &&
      d.getDate() == Number(temp[0]) &&
      d.getFullYear() == Number(temp[2]);

    return isValidDate;
  }

  validateDate(key: string): void {
    if (this.sortFilterForm.controls.startDate.value && key == 'START_DATE') {
      this.isStartDateValid = this.dateValidation(
        this.sortFilterForm.controls.startDate.value,
      );
    }
    if (this.sortFilterForm.controls.endDate.value && key == 'END_DATE') {
      this.isEndDateValid = this.dateValidation(
        this.sortFilterForm.controls.endDate.value,
      );
    }
  }
}
