import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalService } from '../../services/global.service';

@Component({
  selector: 'app-uw-review-proposal',
  templateUrl: './uw-review-proposal.component.html',
  styleUrls: ['./uw-review-proposal.component.scss'],
})
export class UwReviewProposalComponent implements OnInit {
  constructor(public global: GlobalService, private router: Router) {}

  ngOnInit(): void {}

  back() {
    this.router.navigate(['ssb/UwProposalListing']);
  }
}
