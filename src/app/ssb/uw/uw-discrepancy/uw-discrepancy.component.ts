import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  OnInit,
  Output,
  TemplateRef,
} from '@angular/core';
import { FormGroup, FormArray, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { WhiteSpaceValidator } from 'src/app/directives/white-space.validator';
import { GlobalService } from '../../services/global.service';

@Component({
  selector: 'app-uw-discrepancy',
  templateUrl: './uw-discrepancy.component.html',
  styleUrls: ['./uw-discrepancy.component.scss'],
})
export class UwDiscrepancyComponent implements OnInit {
  documentUploadForm: FormGroup;
  dropDownArr = [];

  constructor(
    private _router: Router,
    private _formBuilder: FormBuilder,
    private cd: ChangeDetectorRef,
    public global: GlobalService,
  ) {}

  ngOnInit(): void {
    this.initializeForm();
  }

  initializeForm() {
    this.documentUploadForm = this._formBuilder.group({
      documentArr: this._formBuilder.array([this.addDocumentGroup()]),
    });
  }

  addDocumentGroup(): FormGroup {
    return this._formBuilder.group({
      document: ['', WhiteSpaceValidator.restrictWhiteSpaces],
      documentName: ['', WhiteSpaceValidator.restrictWhiteSpaces],
      remarks: ['', WhiteSpaceValidator.restrictWhiteSpaces],
    });
  }

  onFileChange(event, documentGroup) {
    let reader = new FileReader();

    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      this.setFormFileName(file, documentGroup);
      reader.readAsDataURL(file);

      reader.onload = () => {
        this.cd.markForCheck();
      };
    }
  }

  setFormFileName(file, documentGroup) {
    documentGroup.get('documentName').setValue(file.name);
  }

  removeDocFile(documentGroup) {
    documentGroup.get('documentName').setValue('');
  }

  addDocumentArrBtn() {
    const documentArr = this.documentUploadForm.get('documentArr') as FormArray;
    documentArr.push(this.addDocumentGroup());
  }

  removeDocumentGroupArrBtn(ind) {
    const documentArr = this.documentUploadForm.get('documentArr') as FormArray;
    documentArr.removeAt(ind);
  }

  routeBack() {
    this._router.navigate(['ssb/UwProposalListing']);
  }

  route() {
    this._router.navigate(['ssb/UwProposalListing']);
  }

  routeBackToProposal() {
    this._router.navigate(['ssb/UwProposalListing']);
  }
}
