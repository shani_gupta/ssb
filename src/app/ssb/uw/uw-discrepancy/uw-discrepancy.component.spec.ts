import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UwDiscrepancyComponent } from './uw-discrepancy.component';

describe('UwDiscrepancyComponent', () => {
  let component: UwDiscrepancyComponent;
  let fixture: ComponentFixture<UwDiscrepancyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UwDiscrepancyComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UwDiscrepancyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
