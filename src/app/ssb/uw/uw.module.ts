import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgSelectModule } from '@ng-select/ng-select';
import { TranslateModule } from '@ngx-translate/core';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ToastrModule } from 'ngx-toastr';
import { UwProposalListingComponent } from './uw-proposal-listing/uw-proposal-listing.component';
import { InrcurrencyModule } from 'src/app/inrcurrency/inrcurrency.module';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { UwReviewProposalComponent } from './uw-review-proposal/uw-review-proposal.component';
import { UwDiscrepancyComponent } from './uw-discrepancy/uw-discrepancy.component';

@NgModule({
  declarations: [
    UwProposalListingComponent,
    UwReviewProposalComponent,
    UwDiscrepancyComponent,
  ],
  imports: [
    CommonModule,
    NgSelectModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    NgxPaginationModule,
    //BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot({
      timeOut: 10000,
      preventDuplicates: true,
    }),
    TranslateModule,
    InrcurrencyModule,
    NgMultiSelectDropDownModule,
  ],
})
export class UwModule {}
