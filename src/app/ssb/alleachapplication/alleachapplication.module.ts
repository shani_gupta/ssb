import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AlleachapplicationComponent } from './alleachapplication.component';

import { TabsModule } from 'ngx-bootstrap/tabs';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CustomFormsModule } from 'ngx-custom-validators';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgSelectModule } from '@ng-select/ng-select';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxSpinnerModule } from 'ngx-spinner';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AllEachApplicationRoutingModule } from './alleachapplication-routing.module';

@NgModule({
  declarations: [
    AlleachapplicationComponent
  ],
  imports: [
    AllEachApplicationRoutingModule,
    CommonModule,
    NgSelectModule,
    TypeaheadModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    TabsModule.forRoot(),
    AccordionModule.forRoot(),
    Ng2SearchPipeModule,
    CustomFormsModule,
    NgxSpinnerModule,
    BsDatepickerModule.forRoot(),
    NgbModule,
    NgxPaginationModule,
    ReactiveFormsModule,
  ],
})
export class AllEachApplicationModule {}
