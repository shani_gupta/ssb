import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, TemplateRef } from '@angular/core';
//import { QuoteService } from '../;
//import { http } from 'src/app/insillion/httpng';

import { NgbDateAdapter } from '@ng-bootstrap/ng-bootstrap';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FormBuilder } from '@angular/forms';
//import {  insapi, IProfile } from 'src/app/insillion/insapi';

//import { ProposalService } from '../service/proposal.service';
// import { ApiService } from 'src/app/core/http/api.service';
import { QuoteService } from '../quote/quote.service';
// import { HelperService } from 'src/app/shared/helper/helper.service';
import { ApiService } from '../services/api.service';
import { GlobalService } from '../services/global.service';
@Component({
  selector: 'app-alleachapplication',
  templateUrl: './alleachapplication.component.html',
  styleUrls: ['./alleachapplication.component.sass'],
})
export class AlleachapplicationComponent implements OnInit {
  modalRef: BsModalRef;
  // profile: IProfile = null;
  profile: any = null;
  products: object[] = [];
  myrole: any;
  lobdata: any = [];
  id: any;
  selectedIndex: any;
  alllobdata: any = [];
  particularlobdata: any = [];
  masterlobdata: any = [];
  fianlcorrespondingproduct: any = [];
  alllobiconslist: any = [];
  alllobdatatemp: any = [];

  producticonslist: any = [];
  productsfinallist: any = [];
  current_lob: any;
  role: any;
  selectedLob: string;
  sidebardiv = false;
  featureenable: any = {};
  profileData: any;
  userProduct: any = [];
  formData: any;
  e: any;
  productId: any = 'M100000000014'; // uat
  // productName:any="PAR - Policy";

  productName: any = 'PAR-Policy'; // uat
  // email:any ="rakesh@godbtech.com";
  // pwd:any="544264";

  // email: any = "anand@godbtech.com"; //uat
  // pwd: any = "224476" //uat
  email: any = 'durga@godbtech.com'; //uat
  pwd: any = '601478'; //uat

  // product_url = '/api/v1/product/M000000001012'

  constructor(
    //public parglobals: QuoteService,
    private router: Router,
    private route: ActivatedRoute,
    public formbuilder: FormBuilder,
    private modalService: BsModalService,
    public quote: QuoteService,
    public apiService: ApiService,
    public dateAdapter: NgbDateAdapter<string>,
    public globals: GlobalService,
  ) {}

  async ngOnInit() {
    let user_token = JSON.parse(
      JSON.stringify(sessionStorage.getItem('in-auth-token')),
    ); //for ipdsv2
    //http.storeToken(user_token);
    console.log(user_token, 'token');

    // Auth API
    // await insapi.auth(this.email, this.pwd);

    //this.profile = await insapi.loadUser();
    this.profile = await this.apiService.fetchProfile();
    // this.profile=this.apiService.profileDetails
    console.log('this.profile', this.profile);

    this.role = this.profile?.data?.role;
    console.log('this.role', this.role);
  }
  // callPlan() {
  //   //this.parglobals.modifyplan = 0;
  //   this.router.navigate(['parcreateplan']);
  // }
  callMaster() {
    this.router.navigate(['ssb/master']);
  }
  confirmatinModal(confirmation_template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(
      confirmation_template,
      Object.assign({}, { class: 'theme-modal modal-sm popup-small' }),
    );
  }
  callProducer() {
    //this.quote.producercode_mapping.reset();
    this.router.navigate(['ssb/quote']);
  }

  callQuote() {
    window.location.href =
      this.apiService.searchUrl +
      '?lob_value=personalaccident&stage=Quote&days=';
    console.log('quote Url ', window.location.href);
  }

  callProposal() {
    window.location.href =
      this.apiService.searchUrl +
      '?lob_value=personalaccident&stage=Proposal&days=';
    console.log('Proposal Url ', window.location.href);
  }
  callPlanList() {
    this.router.navigate(['ssb/quotelisting']);
  }
  callProducerList() {
    this.router.navigate(['parmasterpolicymaplist']);
  }
  callBulkUpload() {
    this.router.navigate(['ssb/master/viewBulkUploadStatus']);
  }

  // moveToMain(path) {
  //   if (path == 'allapplication') {
  //     window.location.href = this.api.allApplcationUrl;
  //   } else if (path == 'par') {
  //     window.location.href = this.api.personalLinesUrl;
  //   }
  // }

  // back() {
  //   window.location.href = this.api.personalLinesUrl;
  // }
}
