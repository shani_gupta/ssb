import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AlleachapplicationComponent } from './alleachapplication.component';

//import { FormGuardService } from 'src/app/core/guards/form-guard.service';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: AlleachapplicationComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AllEachApplicationRoutingModule {}
