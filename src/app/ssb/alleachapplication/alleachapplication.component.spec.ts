import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlleachapplicationComponent } from './alleachapplication.component';

describe('AlleachapplicationComponent', () => {
  let component: AlleachapplicationComponent;
  let fixture: ComponentFixture<AlleachapplicationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlleachapplicationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlleachapplicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
