import {
  CUSTOM_ELEMENTS_SCHEMA,
  NgModule,
  NO_ERRORS_SCHEMA,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { MasterModule } from './master/master.module';
import { SsbRoutingModule } from './ssb-routing.module';
//import { ProducerModule } from './producer/producer.module';
import { TranslateModule } from '@ngx-translate/core';
//import { UwModule } from './uw/uw.module';
import { PageNotfoundComponent } from './page-notfound/page-notfound.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgxPaginationModule } from 'ngx-pagination';
import { QuoteModule } from './quote/quote.module';

@NgModule({
  declarations: [PageNotfoundComponent],
  imports: [
    CommonModule,
    MasterModule,
    SsbRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    NgxPaginationModule,
    //ProducerModule,
    ToastrModule.forRoot({
      timeOut: 10000,
      preventDuplicates: true,
    }),
    TranslateModule,
    //UwModule,
    NgMultiSelectDropDownModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class SsbModule {}
