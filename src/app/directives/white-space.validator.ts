import { AbstractControl, ValidationErrors } from '@angular/forms';

export class WhiteSpaceValidator {
  static restrictWhiteSpaces(
    control: AbstractControl,
  ): ValidationErrors | null {
    if (
      (control?.value as string)?.length > 0 &&
      (control.value as string).trim().length == 0
    ) {
      control.setValue(control.value.trim());
    }
    return null;
  }
}
